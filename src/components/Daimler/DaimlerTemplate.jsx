import React from 'react'
import { useEffect,useState} from "react";
import axios from "axios"
var year;
const DaimlerTemplate =(props)=> {

    const [daimlerTemplate,setDaimlerTemplate]=useState([])
    const [isTemplatesView,setisTemplateView]=useState(true);
      
     console.log(props.token,"dhfsghdfsb")
     //console.log(props.daimlerTemplate,"temlate")
     useEffect(()=>{
        if(props.token){
          axios
          .get(
            "https://apstaging.matrixbscloud.com/apis/" + "/auditor/v1/keymanagement/templates?year=" + year,
            {
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${props.token}`,
              },
            }
          )
          .then((response) => {
            console.log(response.data.Data, "inside gettemplates")
            setDaimlerTemplate(response.data.Data);
          })
          .catch((error) => {
            console.log(error.message, "error message")
          });
        }
      },[]);


      const handleTableViewAuditClick =()=>{
          console.log("......")
      }

      var years=[
        {
          value:2018,
          text:"2018",
          key:"2018"
        },
        {
          value:2019,
          text:"2019",
          key:"2019",
        },
        {
          value:2020,
          text:"2020",
          key:"2020",
        },
        {
          value:2021,
          text:"2021",
          key:"2021",
        },
        {
          value:2022,
          text:"2022",
          key:"2022",
        },
      ]
      var columns =[
        {
          Headers:"Template Name",
          accessor:"auditName",
          style:{cursor:"pointer"},
          Cell:row=>(
            <AuditTableCell
             text={row.original.auditName}
             onClick={handleTableViewAuditClick}
             row={row.original}
            />
          )
        },
        {
          Header:"Dealers Count",
          accessor:"dealercount",
          style:{textAlign:'center',cursor:'pointer'},
          Cell:row=>(
            <AuditTableCell
              text={row.original.dealercount}
              onClick={handleTableViewAuditClick}
              row={row.original}
            />
          )
        },
        {
          Headers:'vehicles Count',
          accessor:'vehiclecount',
          style:{cursor:'pointer',whiteSpace:'normal'},
          Cell:row=>(
            <AuditTableCell
            text={row.original.vehiclecount}
            onClick={handleTableViewAuditClick}
            row={row.original}
            />
          )
        }
      ]
    return (
        <div style ={{flexgrow:1,display:'flex',flexFlow:'column'}}>
            {
                isTemplatesView && (
                    <div> 
                        <div style={{paddingLeft:30,flex:" 0 0 30px", display: "inline-block", marginBottom: 20 }}>
                        <h1 style={{ display: "inline-block" }} >Templates</h1>
                        {
                            
                        }
                            </div>
                    </div>
                )
            }
                    
        </div>
    )
}

function AuditTableCell(props) {
  function onClick() {
      props.onClick(props.row);
  }
  return (
      <div style={props.style} onClick={onClick}>
          {props.text}
      </div>
  );
}
export default DaimlerTemplate;
