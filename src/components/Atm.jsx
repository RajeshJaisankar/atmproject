import React, { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios'
import {
  Input,
  Label,
  Divider,
  Modal,
  Image,
  Icon,
  Segment,
  Button,
  Header,
  Grid,
  Select,
} from "semantic-ui-react";
import moment from "moment";
import MailPopup from '../components/MailPopup';
import 'semantic-ui-css/semantic.min.css'
import config from '../config';

import FileSaver from 'file-saver';
import Bank from './Bank';
//import { connect } from 'react-redux';
var master = [];

var storageBaseUrl = config["storage_base_url"];


const Atm = (props) => {
  master = [];
  const [Atmdata, setAtmdata] = useState([]);
  // const [master, setmaster] = useState({});
  const [EditMode, setEditMode] = useState(false);
  const [selectedPhoto, setselectedPhoto] = useState("");
  const [showPopup, setshowPopup] = useState(false);
  const [currentTc, setcurrentTc] = useState(null);
  const [showPhotoSlider, setshowPhotoSlider] = useState(false);
  const [modalOpen, setmodalOpen] = useState(false);
  const [zoomImage, setzoomImage] = useState("");
  const [disabled, setdisabled] = useState(false);
  const [currentAudit, setcurrentAudit] = useState([]);
  const [initialData, setinitialData] = useState([]);
  const [mailpopup, setmailpopup] = useState(false);
  const [user, setuser] = useState({});
  const [photoupload, setphotos] = useState({});


  // console.log(props.selectedUser);
  //console.log(props.templateData);
  // console.log(props.user,"ghcg");
  // console.log(props.selectedUser.atmId,"jddshhfkkfs")
  //console.log(master.questions,"dgshfgsjh")
  //console.log(master._id)
  // console.log(props.user.Audit, "audit")
  if (props.user.Audit !== null) {
    master = props.user.Audit;
    
  }
  useEffect(() => {
  //  setcurrentAudit(master);

  })
  //console.log(master,"line num 64")
  const editAudit = () => {
    setEditMode(true)
    setdisabled(true)
  }
  const CancelEditAudit = () => {
    setEditMode(false)
    setdisabled(false)
  }

  const onDownloadReport = () => {

    // console.log(props.user, "user")
    // console.log(user.Audit._id,"id..")
    // console.log(user.Audit.auditId)
    if (props.token != null) {
      // setApiToken(props.token);
      axios
        .get(

          "http://localhost:2014" + `/auditor/v1/atm/individualReport?auditId=${master._id}`,
          {
            responseType: "blob",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${props.token}`,
            },
          }
        )
        .then((response) => {
          console.log(response.data, "download")
          FileSaver.saveAs(response.data, `${props.selectedUser.atmId}-${props.selectedUser.atmName}.pdf`);
          // console.log(response.data,"atmdata")
        })
    }
    // this.props.downloadAtmAuditReportAction(Id._id,this.props.user.atmId,this.props.user.atmName)
  }

  const SaveAudit = () => {
    // console.log(props.user.Audit._id, "user")
    if (props.token != null) {
      // setApiToken(props.token);
      axios
        .post("http://localhost:2014" + `/auditor/v1/atm/saveAudits`,master,
          {
            responseType: "blob",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${props.token}`,
            },
          }
        ).then((response) => {
         // alert('Data Updated Successfully')
         console.log(response,"..........");
         setEditMode(false);
        })
    }
    
  }

  const download = () => {
    setmailpopup(true)
  }

  const handleMail = () => {
    setmailpopup(false)
  }

  const handleMailchange = (mail) => {

    if (props.token) {
      axios.get("http://localhost:2014" + `/auditor/v1/atm/sendMail?auditId=${master._id}&mailIds=${mail}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${props.token}`,
        },
      }).then((response) => {
        console.log(response.data, "hgkhdsfbf");
      })
    }
  }
  let question = [];

  const atmchangeAnswer = (ques, e) => {
    var answers = "";
    let audit = master;
    if (ques.answerType == "text") {
      answers = e.target.value;
      console.log(answers, "line num 126")
      audit.questions.map(eachQuestion => {
        if (eachQuestion.id == ques.id) {
          eachQuestion.answer = answers;
        }
      });
    }
    if (ques.answerType == "options") {
      answers = e;
      audit.questions.map(eachQuestion => {
        if (eachQuestion.id == ques.id) {
          eachQuestion.answer = String(answers);
        }
      });
    }



    setcurrentAudit(audit);
    //console.log(currentAudit, "currentaudit")
  };

  const changeNotes = (ques, e) => {
    
    // console.log(master, 'audit inside.....')
    var answers = "";
    let audit = master;
    // console.log(audit, 'audit inside.....')
    answers = e.target.value;
    console.log(answers)
    audit.questions.map(eachQuestion => {
     // console.log(eachQuestion,"remarks")
    // console.log(eachQuestion.id,'   ==    ',ques.id) 
     if (eachQuestion.id == ques.id) {
        eachQuestion.remarks = answers;
      }
    });
    setcurrentAudit(audit);
   // console.log(currentAudit,"current")
  }

  const zoomPics = url => {
    setmodalOpen(true)
    setzoomImage(url)
  };

  const closeEditUser = () => {
    setmodalOpen(false)
  }


  const atmdropdownchangeAnswer = (ques, e) => {
    var answers = "";
    let audit = master;
    console.log(audit, "dshf")
    answers = e;
    audit.questions.map(eachQuestion => {
      if (eachQuestion.id == ques.id) {
        eachQuestion.categoryAnswer = String(answers);
      }
    })
    setcurrentAudit(audit);
  }


  const uploadAtmPhotos = (photo, locationCode, { onSuccess, onFailure }) => {
    let file = new FormData();
    file.append('photo', photo)
    console.log(file,"file")
    if (props.token != null) {
      // setApiToken(props.token);
      axios
        .post("http://localhost:2014" + `/auditor/v1/upload/atmPhoto?locationCode=${locationCode}`, file,
          {

            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${props.token}`,
            },
          }
        ).then((response) => {
          console.log(response, "data")
          onSuccess(response)
        }).catch((error) => {
          console.log(error.message, "error message")
        })
      onFailure();
    }
  }

  const uploadPhoto = (ques) => {
    let photo = selectedPhoto;
    let locationCode = props.selectedUser.atmId;
    // console.log(locationCode,"dbhjbfdh")
    console.log(photo,"photo")
    uploadAtmPhotos(photo, locationCode, {
    
      onSuccess: (response) => {

        console.log(response, "response")
        let audit = master;
        console.log(master, "jsfh")
        audit.questions.map(eachQuestion => {
          if (eachQuestion.id == ques.id) {
            console.log('zeroooooooooo')
            if (eachQuestion.photos && eachQuestion.photos.length > 0) {
              console.log('first')

              let data = {
                isUploaded: true,
                uploadedImageUrl: response.data
              }
              console.log(data, "uploadedimage");
              eachQuestion.photos.push(data);
              console.log(eachQuestion.photos, 'threeeeeeeeeeeeeee')
            }
            else {
              console.log('second')
              let data = [
                {
                  isUploaded: true,
                  uploadedImageUrl: response.data
                }
              ]
              eachQuestion.photos = data
            }
          }
        });
        setcurrentAudit(audit);

      },
      onFailure: () => {
        alert("Photo Uploaded");
      },
    });
  };

  const onPhotoSelect = (e) => {
    setselectedPhoto(e.target.files[0])
  };

  const onDownloadPhotos = () => {
    console.log(props.selectedUser, "selected")
    console.log(props.user, "Id")
    console.log(master._id, "auditId")
    if (props.token != null) {
      // setApiToken(props.token);
      axios
        .get(`http://localhost:2014/auditor/v1/atm/zipPhotos?auditId=${master._id}`,
          {
            responseType: "blob",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${props.token}`,
            },
          }
        )
        .then((response) => {
          console.log(response.data, "download")
          FileSaver.saveAs(response.data, `${props.selectedUser.atmId}-${props.selectedUser.atmName}.zip`);
          // console.log(response.data,"atmdata")
        })
    }
    // this.props.downloadAtmAuditPhotosAction(Id._id,this.props.user.atmId,this.props.user.atmName);
  }

  //  const onClose =()=>{
  //    console.log("bhsdbfh")
  //  }

  //console.log(master.questions, 'questions')

  //   console.log(master,"what")

  return (
    <div style={{
      marginRight: "100px",
      marginLeft: "40px",
      paddingRight: "100px"
    }}>
      <Segment
        onClick={props.onClose}
        style={{
          color: "#808080",
          float: "right",
          cursor: "pointer",
          marginTop: -7,
          position: "absolute",
          right: 58
        }}>
        <Icon name="arrow" className="left large" color="brown" />
      </Segment>
      <Modal
        open={modalOpen}
        onClose={closeEditUser}
        size="small"
        style={{ top: "30%", left: "10%", right: "10%", width: "50%", marginTop: "10px" }}
      >
        <Modal.Content>
          <img
            src={storageBaseUrl + zoomImage}
            style={{
              width: "550px",
              height: "20%"
              // padding: "0 10px"
            }}
          />
        </Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={closeEditUser}>
            <Icon name="remove" /> Close
          </Button>
        </Modal.Actions>
      </Modal>
      <Header style={{ color: "orange", fontSize: 25 }}>
        {master.auditName}
      </Header>
      <p> Created at {moment(master.createdAt).format("DD-MM-YYYY  HH:MM:SS")}</p>
      {!EditMode &&
        <Label style={{ cursor: "pointer" }} onClick={editAudit}>
          <Icon name="edit" /> Edit audit
        </Label>}

      {EditMode &&
        <div style={{ marginTop: "6px" }}>
          <Label
            style={{ cursor: "pointer", marginRight: "5px" }}
            onClick={SaveAudit}
          >
            <Icon name="save" /> Save audit
          </Label>
          <Label
            style={{ cursor: "pointer", marginLeft: "5px" }}
            onClick={CancelEditAudit}
          >
            <Icon name="cancel" /> Cancel
          </Label>
        </div>}
      <Label
        color="green"
        style={{ cursor: "pointer" }}
        onClick={() => onDownloadReport()}
      >
        <Icon name="table" />
        Download Report
      </Label>
      <Label
        color="blue"
        style={{ cursor: "pointer" }}
        onClick={download}
      >
        <Icon name="mail square" />
        Send Mail
      </Label>
      <Label
        color="orange"
        style={{ cursor: "pointer" }}
        onClick={() => onDownloadPhotos()}
      >
        <Icon name="camera" />
        Download Photos
      </Label>

      {mailpopup &&
        <MailPopup
          open={mailpopup}
          closePopup={handleMail.bind(this)}
          sendMailAndClosePopup={handleMailchange}
        />
      }


      {master.questions !== undefined && master.questions !== null ?
        <>
          {master.questions.map((ques) => (
            <Grid>
              <Grid.Column style={{ marginTop: 25 }}>
                <Grid.Row>
                  {ques.question}
                </Grid.Row>
                <Grid.Row>
                  {ques.answerType === "text" &&
                    <div>
                      <Input
                        placeholder="Enter Text"
                        value={ques.answer}
                        disabled={!disabled}
                        onChange={(e) =>
                          atmchangeAnswer(ques, e)}
                      />
                      <br></br>
                      <br></br>
                      {ques.dropdown &&
                        <div>
                          <Select
                            placeholder={"Enter Text"}
                            options={ques.dropdown.map((label, i) => {
                              return {
                                value: label.value,
                                text: label.label,
                                key: label.key,
                              };
                            })}

                            value={Number(ques.categoryAnswer)}
                            disabled={!disabled}
                            onChange={(e, { value }) =>
                              atmdropdownchangeAnswer(ques, value)}
                          />

                        </div>
                      }
                      {ques.remarks &&
                        <div>
                          <h4 style={{ fontWeight: 'normal' }}>{'Remarks'}</h4>
                          <Input
                            placeholder="Enter Text"
                            value={ques.remarks}
                            disabled={ !disabled}
                            onChange={(e) => changeNotes(ques, e)}
                          />
                          <Input
                            placeholder="Enter Text"
                            value={ques.remarks}
                             disabled={ !disabled}
                            onChange={(e) => changeNotes(ques, e)}
                          />
                        </div>
                      }
                      <br></br>
                      <br></br>
                      {!ques.remarks &&
                        <div>
                          <h4 style={{ fontWeight: 'normal' }}>{'Remarks'}</h4>
                          <Input
                            placeholder="Enter Text"
                            onChange={(e) => changeNotes(ques, e)}
                            disabled={!disabled}
                          />
                        </div>
                      }
                      <br></br>
                      <br></br>
                      <Image.Group
                        size="mini"
                        style={{ cursor: "pointer" }}
                      >
                        {ques.photos && ques.photos.length > 0 && ques.photos.map((p, i) => {
                          if (p.uploadedImageUrl != null) {
                            return (
                              <Image
                                key={i}
                                src={storageBaseUrl + p.uploadedImageUrl}
                                onClick={() =>
                                  zoomPics(p.uploadedImageUrl)}
                              // onClick={(k => {
                              // return e => this.handlePhotoClick(e, k);
                              // })(p)}
                              />
                            );
                          }
                        })}
                      </Image.Group>
                      <br></br>
                      <br></br>
                      {EditMode == true &&

                        <div>
                          <input
                            type="file"
                            content="Upload additional Photos"
                            icon="cloud upload"
                            labelPosition="right"
                            onChange={(e) => onPhotoSelect(e)}
                            multiple
                            style={{ display: "inline-block", fontSize: 20 }}
                            accept=".jpg,.png,.jpeg"
                          />
                          <Button
                            // style={{ marginTop: "10px", marginLeft: "70%" }}
                            color="green"
                            size="medium"
                            onClick={(e) => uploadPhoto(ques)}
                          >
                            Upload
                          </Button>
                        </div>
                      }


                    </div>

                  }
                  {ques.answerType === "options" &&
                    <div>
                      <Select
                        disabled={!disabled}
                        options={ques.options.map((label, i) => {
                          return {
                            value: label.value,
                            text: label.label,
                            key: label.key,
                          };
                        })}
                        placeholder={"Select any option"}
                        value={Number(ques.answer)}
                        onChange={(e, { value }) =>
                          atmchangeAnswer(ques, value)}
                      />
                      <br></br>
                      <br></br>
                      {ques.dropdown &&
                        <div>
                          <Select
                            placeholder={"Enter Text"}
                            options={ques.dropdown.map((label, i) => {
                              return {
                                value: label.value,
                                text: label.label,
                                key: label.key,
                              };
                            })}

                            value={Number(ques.categoryAnswer)}
                            disabled={!disabled}
                            onChange={(e, { value }) =>
                              atmdropdownchangeAnswer(ques, value)}
                          />
                        </div>
                      }
                       {console.log(disabled,"......")}
                      {ques.remarks &&
                        <div>
                          <h4 style={{ fontWeight: 'normal' }}>{'Remarksssssss'}</h4>
                          <Input
                            placeholder="Enter Text"
                            value={ques.remarks}
                            disabled={!disabled}
                            onChange={(e) => changeNotes(ques, e)}
                          />
                        </div>
                      }
                      <br></br>
                      <br></br>
                      {!ques.remarks &&
                        <div>
                          <h4 style={{ fontWeight: 'normal' }}>{'Remarksee'}</h4>
                          <Input
                            placeholder="Enter Text"
                            disabled={disabled}
                            onChange={(e) => this.changeNotes(ques, e)}
                          />
                        </div>
                      }
                      <br></br>
                      <br></br>
                      <Image.Group
                        size="mini"
                        style={{ cursor: "pointer" }}
                      >
                        {ques.photos && ques.photos.length > 0 && ques.photos.map((p, i) => {
                          if (p.uploadedImageUrl != null) {
                            return (
                              <Image
                                key={i}
                                src={storageBaseUrl + p.uploadedImageUrl}
                                onClick={() =>
                                  zoomPics(p.uploadedImageUrl)}
                              // onClick={(k => {
                              // return e => this.handlePhotoClick(e, k);
                              // })(p)}
                              />
                            );
                          }
                        })}
                      </Image.Group>
                      <br></br>
                      <br></br>
                      {EditMode == true &&

                        <div>
                          <input
                            type="file"
                            content="Upload additional Photos"
                            icon="cloud upload"
                            labelPosition="right"
                            onChange={(e) => onPhotoSelect(e)}
                            multiple
                            style={{ display: "inline-block", fontSize: 20 }}
                            accept=".jpg,.png,.jpeg"
                          />
                          <Button
                            // style={{ marginTop: "10px", marginLeft: "70%" }}
                            color="green"
                            size="medium"
                            onClick={(e) => uploadPhoto(ques)}
                          >
                            Upload
                          </Button>
                        </div>
                      }
                    </div>
                  }
                  {ques.answerType == "photo" &&
                    <div>
                      <Image.Group
                        size="mini"
                        style={{ cursor: "pointer" }}
                      >
                        {ques.photos.length > 0 && ques.photos.map((p, i) => {
                          if (p.uploadedImageUrl != null) {
                            return (
                              <Image
                                key={i}
                                src={storageBaseUrl + p.uploadedImageUrl}
                                onClick={() =>
                                  zoomPics(p.uploadedImageUrl)}
                              // onClick={(k => {
                              // return e => this.handlePhotoClick(e, k);
                              // })(p)}
                              />
                            );
                          }
                        })}

                      </Image.Group>
                      <br></br>
                      <br></br>
                      {EditMode == true &&

                        <div>
                          <input
                            type="file"
                            content="Upload additional Photos"
                            icon="cloud upload"
                            labelPosition="right"
                            onChange={(e) => onPhotoSelect(e)}
                            multiple
                            style={{ display: "inline-block", fontSize: 20 }}
                            accept=".jpg,.png,.jpeg"
                          />
                          <Button
                            // style={{ marginTop: "10px", marginLeft: "70%" }}
                            color="green"
                            size="medium"
                            onClick={(e) => uploadPhoto(ques)}
                          >
                            Upload
                          </Button>
                        </div>
                      }
                    </div>
                  }
                </Grid.Row>
                <Divider />
              </Grid.Column>
            </Grid>
          ))
          }
        </>
        :
        null
      }

    </div>

  )

}



export default Atm;



{/* <Grid>
            <Grid.column style={{ marginTop: 25 }}>
              <Grid.row>
              {ques.question}
              </Grid.row>
            </Grid.column>
          </Grid> */}