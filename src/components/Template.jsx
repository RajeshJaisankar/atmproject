import Login from "./Login"
import { useHistory } from "react-router-dom";
import { useState, useEffect } from "react";
import React from 'react';
//import axios from 'axios';
import "D:/office and atm/atmproject/src/style/style.css";
import ReactTable from "react-table";
import { Sidebar,Menu, Dropdown } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css'
import 'react-table/react-table.css';
import "D:/office and atm/atmproject/src/style/style.css";

import { Segment, Icon } from "semantic-ui-react";

import Bank from "./Bank";
import axios from 'axios'
import FileSaver from "file-saver";

// import Cannon from "./Canon/Canon"
// import tanishq from "./Tanishq/Tanishq"
// import Daimler from "./Daimler/Daimler"
// import  agentverification from "./agentverification/agentverification"
// import Damages from "./Damages/Damages"
// import hersheys from "./hersheys/hersheys"
// import Naturals from "./Naturals/Naturals"
// import pandg from "./pandg/pandg"
// import furlenco from "./furlenco/furlenco"
// import godrej from "./godrej/godrej"
// import preethi from "./preethi/preethi"
// import Atm from "./Atm"; 
// import udaan from "./udaan/udaan"
// import volkswagen from "./Volkswwagen/Volkswwagen"
// import mrRetail from "./MrRetail/mrreatil"
const Template = (props) => {
    //var arr;
    //var filteredArr;
    let history = useHistory();
    const [thisTemplateView, setThisTemplatesView] = useState(true);
    const [thistempalteView, setthistemplateView] = useState(true);
    const [selectedTemplate, setselectedTemplate] = useState(true);
    //    const [count, setcount] = useState({});
    //  const [Indata, setIndata] = useState(null);
    const [templateData, settemplateData] = useState([]);
    const [Error, setError] = useState("");
    const [sideBarVisible, setSideBarVisible] = useState(true);
    const [page, setpage] = useState(false);
    //const [template, setTemplate] = useState({});
    const [master, setMaster] = useState({})
    const [masterCount, setMasterCount] = useState([]);
    const [Atmuser,setAtmUser]=useState([]);
  //  const  [AtmUser,setAtmuser]=useState({});
    // console.log(props.token, "token from before page")
    
   // console.log(props.selectedTemplate[0]._id,"fjhgkdgbk")
    useEffect(() => {
        
        settemplateData(props.selectedTemplate);
        if(props.token){
            axios.get("http://localhost:2014/auditor/v1/atm/getAtmUsers",{
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${props.token}`,
                },
            }).then((response) => {
              //  console.log(response.data.Users, "atmuser")
                
                setAtmUser(response.data.Users)
            })
        }
    }, []);

//console.log(templateData,"sfsgdsfgdt")
    const toggleSidebar = (props) => {
        //console.log("toggle")
        setSideBarVisible(!sideBarVisible);
    }

    
    const logout = () => {
        console.log("logout")
        setpage(!page);
        history.push("/")
    }


    const openControlPanel = () => {
        console.log("openControlPanel");
        history.push('/controlpanel')
    }

    
    const handleTableViewAuditClick = (template) => {

     //   console.log(template, "template......");
        setselectedTemplate(template);

        if (props.token) {
            axios
                .get(
                    "http://localhost:2014" + `/auditor/v1/atm/getAllMaster?templateId=${template._id}`,
                    {
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: `Bearer ${props.token}`,
                        },
                    }
                )
                .then((response) => {
                  //  console.log(response.data,"dfsgjf")
                    setMaster(response.data);
                    setMasterCount(response.data.Count)
                    setThisTemplatesView(false);
                    
                    
                    //}
                }).catch((error) => {
                    console.log(error.message, "error message")
                })
            
        }
    }

    const handleCloseClick = () => {

        setThisTemplatesView(true);
    }

    const downloadReport = () => {
        if (props.token) {
            // setApiToken(props.token);
            // console.log(props.token,"qqqqq")
            axios
                .get(
                    "http://localhost:2014"+ `/auditor/v1/atm/generateConsolidatedReport?templateId=${props.selectedTemplate[0]._id}`,
                    {
                        responseType: "blob",
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: `Bearer ${props.token}`,
                        },
                    }
                )
                .then((response) => {
                    console.log(response.data, "download report")
                    FileSaver.saveAs(response.data, "AtmReport.xlsx");
                })
                .catch((error) => {
                    setError(error.message);
                });
        }

    }

    const columns = [
        {
            Header: "Template Name",
            accessor: "Templatedatas",
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.auditName}
                    onClick={handleTableViewAuditClick}
                />
        },
        {
            Header: "Report",
            accessor: "Repaortdata",
            width: 150,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row => {
                return (
                    <Icon
                        size="large"
                        color="green"
                        name="file excel outline"
                        onClick={() => downloadReport(row.original)}
                    />
                );
                return <AuditTableCell row={row.original} text="" />;
            }
        },
    ];

    // console.log(props.name, "name of the person")


    if (page) {
        return (
            <Login />
        )
    }
    else {
        return (
            <div style={{ height: "100%" }} >
                



                {thisTemplateView &&
                    <div>
                        <h2 style={{ paddingLeft: 30, flex: "0 0 30px" }}>Template</h2>
                        <div style={{ display: "flex", flexGrow: 1, flexFlow: "columnn" }}>
                            <ReactTable
                                nodataText="we couldn't find anything"
                                filterable={true}
                                defaultPageSize={20}
                                sortable={true}
                                style={{ height: "85%", width: "180vh", marginLeft: "40px" }}
                                columns={columns}
                                onClick={handleTableViewAuditClick}
                                data={templateData.length !== 0 ? templateData : []}
                            />
                        </div>
                    </div>}{!thisTemplateView &&
                        <Bank selectedTemplate={selectedTemplate} masterCount={masterCount} templateData={templateData} token={props.token} Master={master} Atmuser={Atmuser} onClose={handleCloseClick} />
                }
            </div>
        );
    }
}


function AuditTableCell(props) {
    function onClick() {
        props.onClick(props.row);
    }
    return (
        <div style={props.style} onClick={onClick}>
            {props.text}
        </div>
    );
}

export default Template
