import React from 'react'
//import config from "../config";
import {
    Segment,
    Grid,
    Dropdown,
    Statistic
} from "semantic-ui-react";
import Atm from "./Atm.jsx";
import { useState } from 'react';
import { useEffect } from 'react';
import ReactTable from 'react-table';
import CheckboxView from './CheckboxView.jsx';
import axios from 'axios'
import {
    Icon,
    Modal,
    Button,
    Step,
    Label,
    GridColumn
} from "semantic-ui-react";
import "react-notifications/lib/notifications.css";
import {
    NotificationContainer,
    NotificationManager
} from "react-notifications";
import selectTable from "react-table/lib/hoc/selectTable";

//import * as types from '../actions/types';
// const CheckboxTable = selectTable(ReactTable);

var customers=[];


const Bank = (props) => {
    
    const [Indata, setIndata] = useState(null);
    const [CustomerTableView, setCustomerTableView] = useState(true);
    const [selectedUser, setselectedUser] = useState({})
    const [master, setmaster] = useState([])
    const [rawMaster, setrawMaster] = useState([]);
    const [selectedState, setselectedState] = useState("");
    const [selectedCity, setselectedCity] = useState("");
    const [selectedStatus, setselectedStatus] = useState("");
    const [AtmUser,setAtmUser]=useState([])
    const [masterdata, setmasterdata] = useState(null);
    const [a, setA] = useState("maris")
    const [previouspage, setpreviouspage] = useState(true);
    const [count, setcount] = useState({});
    const [selectAll, setselectAll] = useState(false)
    const [modalOpen, setmodalOpen] = useState(false);
    const [selection, setselection] = useState([]);
    const [userId, setUserID] = useState([]);
    const [checked, setChecked] = useState([])
    const [allcustomer, setallcustomer] = useState([]);
    const [tick, settick] = useState([])
    const [check,setUcheck]=useState([]);
    const [userselectAll,setUserSelectAll] = useState(false)
    const [userChecked,setUserChecked] = useState([])
    const [assignId,setAssignid]=useState([])
    const [assignname, steAssignName] = useState(null)
    const [assignemailid,setAssignEmailId]=useState([])
   // const [customer,setcustomer]=useState([]);
    // const [CheckboxTable, setAtmcheck] = useState(null);
    // console.log(props.token, "token in bank page")


    //console.log(props.selectedTemplate, "selectedtemplate")
    // console.log(count,"count details")

    // console.log(props.Master, 'My data')
    // console.log(props.Master,"filterdata in bank");

    //console.log(props.Master.data,"bank line")
    //console.log(props.Master.Data, "   my data only................................");
    //console.log(props.templateData[0]._id,"jjbdshksbfjhv")

    // console.log(props.Atmuser,"hbdhkbhckz")
    let templateId = props.templateData[0]._id;
    // console.log(props.Master.data,"line num t65")
    //cosole.log(rawMaster, "jdvfsahjfkvbsfhkb")
    useEffect(() => {

        if ((props.Master.Count && Object.keys(props.Master.Count).length)) {
            //console.log(props.Master.Count, "  line num 45");
            setcount(props.Master.count);
        }

        setrawMaster(props.Master.Data);
        setmaster(props.Master.Data);
       // setcustomer(props.Master.Data);


    }, [props.Master]);

    useEffect(()=>{
        if(props.Atmuser !==undefined && props.Atmuser.length !== 0 ){
            setAtmUser(props.Atmuser)
        }
    
    },[props.Atmuser])


    // useEffect(()=>{
    //     if(checked.length !==0){
    //         setTimeout(() => {
    //             setChecked(checked);
    //           }, 3000);
    //     }
    // },[checked])


    const addNewUser = () => {
        setmodalOpen(true)
       // console.log(tick, 'selectionssssss');
       // console.log(checked,"hfjg")
         
    }



    const onStateChange = (e, Combo) => {
        setselectedState(Combo.value)
        setselectedCity("")
        setselectedStatus("")
    }
    const onStatusComboChange = (e, Combo) => {
        setselectedStatus(Combo.value)

    }

    const onCityComboChange = (e, Combo) => {
        setselectedCity(Combo.value)

    }

    const onViewDetailsClick = () => {
        //  console.log(props.selectedTemplate, "selectedtemplate  line 82 ")
        //console.log(props.selectedTemplate._id, "selectedtemplate  line 83 ")
        let state = selectedState;
        let city = selectedCity;
        let status = selectedStatus;
        console.log(state, city, status, "line number 91")
        //console.log(rawMaster,"line 90");
        // var filterMaster = [];
        // if (status === "All" && city === "All" && state === "All") {
        //     filterMaster = rawMaster;
        // }
        // else if (state !== 'ALL' && city === "All" && status !== 'All') {
        //     console.log("Data All Data")
        //     filterMaster = rawMaster.filter(obj => obj.state === state && obj.status === status);
        // }
        // else if (state !== 'All' && city === "All" && status === "All") {
        //     console.log("Data All All")
        //     filterMaster = rawMaster.filter(obj => obj.state === state);
        //     console.log(filterMaster, "line num 101");
        // }
        // else if (state !== "All" && city !== "All" && status === "All") {
        //     filterMaster = rawMaster.filter(obj => obj.state === state && obj.city === city);
        //     console.log(filterMaster, "line num 109");
        // }
        // else if (state !== "All" && city !== "All" && status === status) {
        //     filterMaster = rawMaster.filter(obj => obj.state === state && obj.city === city);
        //     console.log(filterMaster, "line num 109");
        // }
        // else {
        //     filterMaster = rawMaster.filter(obj => obj.state === state && obj.city === city && obj.status === status);
        // }
        // setmaster(filterMaster);
        filteratm(templateId, state, city, status)
    }

    const filteratm = (templateId, state, city, status) => {
        //console.log(props.token,"fddsbvhs")
        if (props.token) {
            axios
                .post("http://localhost:2014" + '/auditor/v1/atm/filter', { templateId: templateId, state: state, city: city, status: status }, {
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${props.token}`,
                    },
                }).then((response) => {
                    // console.log(response.data.Data,'filtered data');
                    setmaster(response.data.Data)
                }).catch((error) => {
                    console.log(error);
                })
        }

    }


    const AssignAudits = (locationId, UserId) => {
        // console.log("rajesh", props.token)
       console.log(locationId, UserId,"assign function")
        if (props.token) {
            axios.post("http://localhost:2014" + '/auditor/v1/atm/assignAudits', {locationId :locationId,userId:UserId}, {
                
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${props.token}`,
                },
            }).then((response) => {
                console.log(response, "Assign Audits");
                
            }).catch((error) => {
                console.log(error, "error")
            })
        }
        setmodalOpen(false)
    }

    const Onclose = () => {
        setCustomerTableView(true)
    }

    const AtmhandleTableViewAuditClick = (user) => {
        setA("suresh");
        // console.log(a,"first onclick")
        let master = [];
        setselectedUser(user)
        setCustomerTableView(false)
        if (props.token != null) {
            // setApiToken(props.token);
            axios
                .get(
                    "http://localhost:2014" + `/auditor/v1/atm/getAudit?locationId=${user._id}`,
                    {
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: `Bearer ${props.token}`,
                        },
                    }
                )
                .then((response) => {
                    //  console.log(response.data, "atmdata")

                    setmasterdata(response.data)
                })
        }
    }

    const removeDuplicates = e => {
        let newArray = [];

        // Declare an empty object
        let UniqueObject = {};

        // Loop for the array elements
        for (let i in e) {
            // Extract the title
            let objTitle = e[i]["text"];

            // Use the title as the index
            UniqueObject[objTitle] = e[i];
        }

        // Loop to push unique object into array
        for (let i in UniqueObject) {
            newArray.push(UniqueObject[i]);
        }

        return newArray;
    }

    const searchFilter = (filter, row) => {
        let value = filter.value && filter.value.toLowerCase();

        if (filter.id) {

            var flatedData = JSON.stringify(
                row._original[filter.id] ? row._original[filter.id] : ""
            );
            return flatedData.toLowerCase().includes(value);
        }
    }

    const UsertoggleSelection = (key) => {
       
    //  var alluser=[];
    //   // console.log(Userlist,"userlist")
    //    AtmUser.forEach(function(e){
    //        alluser.push(e._id)
    //    })
    //    //console.log(AtmUser,"users")
    //   console.log(AtmUser[index],"full data");
    //   console.log(AtmUser[index]._id,"1 data");
    // //    var checkcopy =[...alluser];
    // //    checkcopy[index]=!alluser[index];
    // //     console.log("outside");
       
    // //     if(checkcopy[index]=== false){
    // //         console.log("if");
    // //         setselectAll(false);
    // //         setUcheck(checkcopy)
    // //     }
    // //     else if(checkcopy[index]=== true){
    // //         console.log("else");
    // //         setselectAll(false)
    // //         setUcheck(checkcopy)
    // //     }

    //   var temp=[]
    //   temp=userId;
    //    temp.push(AtmUser[index]._id)
    //    console.log(temp,"temp");
    //      setUserID(temp);
    //     var checkedCopy = [...userChecked];
    //     checkedCopy[index] = !userChecked[index]

    //      console.log(checkedCopy[index],"index");
    //     if (checkedCopy[index] === false) {
    //         setUserSelectAll(false)
    //         setUserChecked(checkedCopy)
    //     }
    //     else if (checkedCopy[index] === true) {
            
    //         setUserSelectAll(false)
    //         // console.log(checkedCopy[index],checked[index],!checked[index],'checkingggggggggg')
    //         setUserChecked(checkedCopy)
    //     }
       
      //form suresh
      // console.log(props.cannonStores.stores, "stores")
      console.log(key, "key")
      // console.log(props.Token, "token")
      // start off with the existing state
      console.log(assignId,"assignID")
      
      var SelectedData = assignId;



      var storeIds = [];
      // SelectedData.map(assignId => {
      //     storeIds.push(assignId);
      // });
      storeIds.push(assignId)
      console.log(SelectedData," selected data")
      console.log(storeIds, "storeIds")

      var locationId=[];
      let unique = [...new Set(assignId)];
      console.log(unique, "unique")

     let temp=[key.slice(7)];

      var data = {
          locationId: unique,
          userId : temp
      };
       console.log(data, "data")
       
       AssignAudits(data);
       locationId.pop();
       console.log(locationId,"location")

    }

    const toggleSelection = (index) => {
        var alldata=[]
        customers.forEach(function (e){
            alldata.push(e._id)
        })
       // console.log(alldata,"All data"
        console.log(index, "index.....", alldata[index], "line num 309");
     
          
        var checkedCopy = [...checked];
        checkedCopy[index] = !checked[index]
        var temp =[];
        temp=tick;
        temp.push(alldata[index])
            settick(temp)
        if (checkedCopy[index] === false) {
            setselectAll(false)
            setChecked(checkedCopy)
        }
        else if (checkedCopy[index] === true) {
            
            setselectAll(false)
            // console.log(checkedCopy[index],checked[index],!checked[index],'checkingggggggggg')
            setChecked(checkedCopy)
        }
       // console.log(checkedCopy, "checkcopy line 299")
       // console.log(checkcopy, "new check")
       
    }

    const usertoggleAll = ()=>{
        const selectAll = !userselectAll;
        setUserSelectAll(!userselectAll);
        var checkedCopy = [];
        AtmUser.forEach(function (e, index) {
        
            checkedCopy.push(!userselectAll);
        });       
        console.log(checkedCopy,'userr')
         setUserChecked(checkedCopy)
        
        

    } 
    const toggleAll = () => {
        const SelectAll = !selectAll
        let allCustomerId = []
        setselectAll(!selectAll);
        var checkedCopy = [];
        customers.forEach(function (e, index) {
            allCustomerId.push(e._id)
            checkedCopy.push(!selectAll);
        });       
         setChecked(checkedCopy)
        
        setallcustomer(allCustomerId);

        
       
    }

    function filterCaseInsensitive(filter, row) {
        const id = filter.pivotId || filter.id;
        return (
            row[id] !== undefined ?
                String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
            :
                true
        );
    }

    const isSelected = (key) => {
        // console.log(key,"hgjv")
        selection.includes(key)
    }



    const userAssign = () => {
        console.log(tick, "selection")
       
        var selectedData = assignId;
       
        var locationId = selectedData;
        var UserId = assignemailid.toString();
        console.log(locationId,UserId,"in assign audits")
        // selectedData.map(e => {
        //     locationId.push(e.slice(7));
        // });
        // userSelection.map(e => {
        //     console.log(e, "e")
        //     UserId.push(e.slice(7));
        // });
        // locationId=selectedData.selection;
        // UserId=userSelection.UserId;
        // console.log(locationId[0],UserId[0],"dbfhsdbjh")
        var temp =[];
        temp.push(UserId);
        console.log(temp,"temp in array");
        UserId=temp;
        AssignAudits(locationId, UserId)
        
       // setmodalOpen(false)
    }
   // console.log(selectAll,'customerselect',userselectAll,'userselect')
   const handleClick = (e, id) => {

        const { name, checked } = e.target; //object destructuring
        
        console.log(name, checked, "seee")
        if (checked === true) {
            assignId.push(name)
            console.log(name, "seee  467")
        }
        if (checked === false) {
            let checkdata = assignId
            let index = checkdata.indexOf(name)
            if (index != -1) {
                checkdata.splice(index, 1)
                setAssignid(checkdata)
            }
        }
        if (name === "selectAll") {
            let tempUser = customers.map((user) => {
                console.log(user, "user")
                return { ...user, isChecked: checked };
                
            });
            setmaster(tempUser);
        } else {
            let tempUser = customers.map((user) =>
                user._id === name ? { ...user, isChecked: checked } : user
            );
            console.log(name, checked, "seee 480")
            setmaster(tempUser);
             
        }
        console.log("clicked....")
        
    } 
    
   const columns = [
        
        // {
        //     Header: (
        //         <input
        //             type="checkbox"
        //             onChange={toggleAll}
        //             checked={selectAll}
        //         />
        //     ),
        //     Cell: row => (

        //         <input
        //             type="checkbox"    
        //             // defaultChecked={checked[row.index]}
        //             checked={checked[row.index]}
        //             onChange={() => { toggleSelection(row.index, checked) }}
        //         />
        //     ),
        //     sortable: false,
        //     filterable: false
        // },
        {   width:35,
            Header: <input
                type="checkbox"
                name="selectAll"
                checked={!customers.some((user) => user?.isChecked !== true)}
                onChange={handleClick}
            />,
            Cell: (row) => (
    
              //  <Checks row={row} change={handleClick} />
    
                <input
                    type="checkbox"
                    name={row.original._id}
                    // defaultChecked={checked[row.index]}
                    checked={row.original?.isChecked || false}
                    onChange={handleClick}
                />
            ),
            sortable: false,
            filterable: false
        },
        {
            Header: "ATM Id",
            accessor: "atmId",

            width: 100,
            style: { textAlign: "left", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.atmId}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },
        {
            Header: "ATM Name",
            accessor: "atmName",
            width: 250,
            style: { textAlign: "left", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.atmName}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },

        {
            Header: "Base Branch",
            accessor: "baseBranch",
            width: 200,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.baseBranch}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },
        {
            Header: "Branch Code",
            accessor: "branchCode",
            width: 200,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.branchCode}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },

        {
            Header: "Status",
            accessor: "status",
            width: 200,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.status}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },
        {
            Header: "Assignee",
            accessor: "assigneeName",
            width: 126,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.assigneeName}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },

    ];


    const handleClickonassign = (e) => {
        const { name, checked } = e.target;
        console.log(name, "name")
        console.log(checked, "checked")
       // setAssignCheckMethod(checked)


        if (checked === true) {

            assignemailid.push(name)

        }

        console.log(assignemailid, "assignmailid")

        if (checked === false) {
            let checkdata = assignemailid
            let index = checkdata.indexOf(name)
            if (index != -1) {
                checkdata.splice(index, 1)
                setAssignEmailId(checkdata)
            }
        }


        if (name === "selectAll") {
            let tempUser = AtmUser.map((user) => {
                console.log(user, "user")
                // setAssignId(user._id)
                // assignId.push(user._id)
                return { ...user, isChecked: checked };


            });
            setAtmUser(tempUser);
        } else {
            let tempUser = AtmUser.map((user) =>
                user._id === name ? { ...user, isChecked: checked } : user
            );
            setAtmUser(tempUser);
        }
    }
    const column = [
        // {
            
        //     Header: (
        //         <input
        //             type="checkbox"
        //             onChange={usertoggleAll}
        //             check={userselectAll}
        //             style={{marginLeft:-330}}
        //         />
        //     ),
        //     Cell: row => (

        //         <input
        //             type="checkbox"
        //             // defaultChecked={checked[row.index]}
        //             checked={userChecked[row.index]}
        //             onChange={() => { UsertoggleSelection(row.index) }}
        //             style={{marginTop:10,marginLeft:10,width:10}}
        //         />
        //     ),
        //     sortable: false,
        //     filterable: false
        // },
        {
            width: 25,
            Header: <input
                type="checkbox"
                name="selectAll"
                checked={!AtmUser.some((user) => user?.isChecked !== true)}
                onChange={handleClickonassign}
            />,
            Cell: (row) => (

                // <Checks row={row} change={handleClick} />

                <input
                    type="checkbox"
                    name={row.original._id}
                    // defaultChecked={checked[row.index]}
                    checked={row.original?.isChecked || false}
                    onChange={handleClickonassign}
                />
            ),
            sortable: false,
            filterable: false
        },
        {
            Header: "Email Id's",
            accessor: "email",
            
            Cell: row =>

                <AuditTableCell row={row.original} text={row.original.email} />
        }
    ];



    
    if (master === undefined) {
        customers = master;
    } else {
        customers = master;
    }
    // console.log(customers, "the master")
    var Userlist = props.Atmuser;

    var city = [{ text: "All", value: "All" }];
    var state = [{ text: "All", value: "All" }];
    var status = [{ text: "All", value: "All" }];
    let uniqueCity;
    let uniqueState;
    let uniqueStatus;
    if (props.Master.Data !== undefined) {
        var data = props.Master.Data;
        data.map((e) => {
            city.push({ text: e.city, value: e.city })
            state.push({ text: e.state, value: e.state })
            status.push({ text: e.status, value: e.status })
        })
    }

    if (city.length > 0) {
        uniqueCity = removeDuplicates(city);
    }
    if (status.length > 0) {
        uniqueStatus = removeDuplicates(status);
    }
    if (state.length > 0) {
        uniqueState = removeDuplicates(state);
    }


    const closeEditUser = () => {
       console.log("close....")
        setmodalOpen(false)
    }
    return (
        <div
            style={{
                flexGrow: 1,
                display: "flex",
                flexFlow: "column"
            }}
        >
            {/* {console.log(modalOpen,".........")} */}
            <NotificationContainer />
            {/* {CustomerTableView && */}
            {CustomerTableView &&
                <div>
                    <div style={{ padding: 15, display: "inline-block" }} >
                        <Segment
                            onClick={props.onClose}
                            style={{
                                color: "#808080",
                                float: "right",
                                cursor: "pointer",
                                marginTop: 7,
                                position: "absolute",
                                right: 58
                            }}
                        >
                            <Icon name="arrow" className="left large" color="brown" />
                        </Segment>
                        <h1
                            style={{
                                paddingLeft: 30,
                                color: "orange",
                                display: "inline-block"
                            }}
                        >
                            ATM Audit
                        </h1>
                    </div>
                    <Grid style={{ marginLeft: "30px" }}>
                        <Grid.Row columns={5}>
                            <Grid.Column>
                                <a
                                    className="ui teal left ribbon label"
                                    style={{ marginLeft: 15 }}
                                >
                                    State
                                </a>
                                <Dropdown
                                    placeholder="Select Region"
                                    fluid
                                    search
                                    selection
                                    options={uniqueState}
                                    value={selectedState}
                                    onChange={onStateChange}
                                />
                            </Grid.Column>
                            <Grid.Column>

                                <a
                                    className="ui teal left ribbon label"
                                    style={{ marginLeft: 15 }}
                                >
                                    City
                                </a>
                                <Dropdown
                                    placeholder="Select City"
                                    fluid
                                    search
                                    selection
                                    options={uniqueCity}
                                    value={selectedCity}
                                    onChange={onCityComboChange}
                                />
                            </Grid.Column>
                            <Grid.Column>
                                <a
                                    className="ui teal left ribbon label"
                                    style={{ marginLeft: 15 }}
                                >
                                    Status
                                </a>
                                <Dropdown
                                    placeholder="Select Status"
                                    fluid
                                    search
                                    selection
                                    options={uniqueStatus}
                                    value={selectedStatus}
                                    onChange={onStatusComboChange}
                                />
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <Label
                                    color="orange"
                                    style={{
                                        cursor: "pointer",
                                        padding: "10px",
                                        marginTop: "26px"
                                    }}
                                    onClick={onViewDetailsClick}
                                >
                                    View Details
                                </Label>
                            </Grid.Column>
                            <Grid.Column>
                                <Label
                                    color="green"
                                    style={{
                                        cursor: "pointer",
                                        padding: "10px",
                                        marginTop: "26px"
                                    }}
                                    tag
                                    pointing="right"
                                    onClick={addNewUser}
                                >
                                    <Icon name="add" /> Assign Audits
                                </Label>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    {/* { Object.keys(props.Master.Count).length &&
                        <div> 
                        Rajesh
                    </div>} */}
                    <div style={{ marginTop: 10 }}>
                        {/* <h1>{
                              Object.keys(count).length 
                            } </h1> */}
                        {
                            props.Master.Count !== undefined &&

                            //console.log(props.Master.Count,"ghfgcfg")
                            <Statistic.Group
                                size={"tiny"}
                                widths={Object.keys(props.Master.Count).length}
                            >
                                {
                                    Object.keys(props.Master.Count).map(key => {
                                        // console.log({ key });
                                        //console.log(props.Master.Count[key])
                                        return (

                                            <Statistic color="blue">
                                                <div style={{ fontSize: 15, marginLeft: '40%', color: "black" }}> {key} </div><br/>
                                                <div style={{ fontSize: 30, marginLeft: '40%', color: "blue" }}> {props.Master.Count[key]} </div>
                                            </Statistic>
                                        );
                                    })
                                }
                            </Statistic.Group>
                        }


                    </div>
                    <div
                        style={{
                            display: "flex",
                            flexGrow: 1,
                            flexFlow: "column",
                            marginTop: 20
                        }}>
                        <ReactTable
                            noDataText="we couldn't find anything"
                            filterable={true}
                            defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row) }
                            defaultPageSize={20}
                            sortable={true}
                        style={{
                                height: "85%", width: "100%", marginLeft: 30
                            }}
                            columns={columns}
                            data={master}
                        />
                        {/* <CheckboxView
                            defaultFilterMethod={searchFilter}
                            data={customers}
                            columns={columns}
                            myRef={setAtmCheckboxRef}
                            toggleSelection={toggleSelection}
                            className="-striped -highlight"
                            selectAll={selectAll}
                            toggleAll={toggleAll}
                            isSelected={isSelected}
                        // getTrProps={this.rowFn}
                        /> */}
                        {/* checkbox view*/}
                    </div>
                </div>}
            {
                !CustomerTableView && masterdata !== null &&
                <Atm token={props.token} templateData={props.Master.Data} selectedUser={selectedUser} user={masterdata} onClose={Onclose} />
            }

            <Modal style={{ top: "20%", left: "10%", right: "10%", width: "70%", marginTop: "7px" }}
                open={modalOpen}
                onClose={()=>closeEditUser}
                size="small"
            >
                <Modal.Content>
                    {/* <h1>Rajesh</h1> */}
                    <ReactTable
                         noDataText="we couldn't find anything"
                         filterable={true}
                         defaultPageSize={20}
                         sortable={true}
                     style={{
                             height: "85%", width: "85%", marginLeft: 30
                         }}
                        data={AtmUser}
                        columns={column}
                        
                        className="-striped -highlight"
                       
                    />
                    {/* <CheckboxView
                        defaultFilterMethod={searchFilter}
                        data={AtmUser}
                        columns={column}
                       // myRef={setAtmCheckboxRef}
                        toggleSelection={UsertoggleSelection}
                        className="-striped -highlight"
                        selectAll={selectAll}
                        toggleAll={toggleAll}
                        isSelected={isSelected}
                    // getTrProps={this.rowFn}
                    /> */}
                </Modal.Content>
                <Modal.Actions>
                    <Button color="red" onClick={closeEditUser}>
                        <Icon name="remove" /> Close
                    </Button>
                    <Button color="black" onClick={userAssign}>
                        Save
                    </Button>
                </Modal.Actions>
            </Modal>
        </div>
    )

}

function AuditTableCell(props) {
    function onClick() {
        props.onClick(props.row);
    }
    return (
        <div style={props.style} onClick={onClick}>
            {props.text}
        </div>
    );
}
export default Bank;