import React, { useState, useEffect } from "react";
import Login from "./Login";
import "D:/office and atm/atmproject/src/style/style.css";
import { Segment, Icon } from "semantic-ui-react";
import 'react-table/react-table.css';
import 'semantic-ui-css/semantic.min.css'
import {
  Sidebar,
  Menu,
  Dropdown,
} from "semantic-ui-react";
import logo from "D:/office and atm/atmproject/src/images/logo.png"
import logotwo from "D:/office and atm/atmproject/src/images/matrix-logo.png";
import { useHistory } from "react-router-dom";
import Preethi from "./preethi/preethi"
import Tanishq from "./Tanishq/Tanishq"
import godrej from "./godrej/godrej"
import hersheys from "./hersheys/hersheys"
import mrreatil from "./MrRetail/mrreatil"
import furlenco from "./furlenco/furlenco"
import Damages from "./Damages/Damages"
import DaimlerTemplate from "../components/Daimler/DaimlerTemplate";
import Canon from "./Canon/Canon"
import udaan from "./udaan/udaan"
import grb from "./grb/grb"
import pandg from "./pandg/pandg"
import Volkswwagen from "./Volkswwagen/Volkswwagen"
import preethi from "./preethi/preethi";
import Template from "./Template";
import axios from "axios";
import Dashboard from "../components/Dashboard/Dashboard"
import KeyManagement from "../components/KeyManagement/Template"

const Home = (props) => {
  const [sideBarVisible, setSideBarVisible] = useState(true);
  const [page, setpage] = useState(false);
  const [name, setName] = useState("");
  let history = useHistory();
  const [selectedTemplate, settemplateData] = useState([]);
  const [daimlerTemplate, setDaimlerTemplate] = useState([])
  // console.log(props.Token, "Token")
  // console.log(props.api.orgs, "api organisation details in template page")
  // console.log(props.dash, "dash...")
  useEffect(() => {
    if (props.token) {
      axios
        .get(
          "http://localhost:2014" + "/auditor/v1/atm/getTemplate",
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${props.token}`,
            },
          }
        )
        .then((response) => {
          //console.log(response.data.Data, "inside gettemplates")
          settemplateData(response.data.Data);
        })
        .catch((error) => {
          console.log(error.message, "error message")
        });
    }
  }, [])



  const openControlPanel = () => {
    history.push('/controlpanel')
  };

  const logout = () => {
    console.log("logout")
    setpage(!page);
    history.push("/")
  }

  const toggleSidebar = () => {
    setSideBarVisible(!sideBarVisible);
  };


  const handleItemClick = (e, { name }) => {
    //console.log(name,"name of template selected")
    setName(name)

  };

  // console.log(props.orgs,"orgs")
  // console.log(props.selectedMenu,"selectedmenu")
  var activeItem = props.selectedMenu || "";
  var pusherStyle = { height: "100%", overflow: "auto", width: "85%" };

  if (!sideBarVisible) {
    pusherStyle.width = "100%";
  }

  if (page) {
    return (
      <Login />
    )
  }
  else {
    return (
      <div style={{ height: "100%" }}>
        <Segment raised style={{ backgroundColor: "#fafafa", height: 60 }}>
          <div style={{ display: "inline-block" }}>
            <Icon
              style={{
                display: "inline-block",
                cursor: "pointer",
                float: "left",
                color: "#606060",
                marginTop: 4
              }}
              onClick={toggleSidebar}
              size="big"
              name="bars"
            />
            <img
              style={{
                height: 120,
                marginTop: -40,
                resize: "horizontal",
                float: "left",
              }}
              src={logo}
            />
            <img
              style={{
                height: 60,
                marginTop: -13,
                float: "left",
              }}
              src={logotwo}
            />
          </div>
          <div
            style={{
              display: "inline-block",
              float: "right",
              paddingRight: 50
            }}
          >
            <Menu.Menu
              style={{ display: "inline", float: "right", marginTop: 8 }}
            >
              <Dropdown pointing text={props.name}>
                <Dropdown.Menu>
                  <Dropdown.Item onClick={openControlPanel}>
                    Control Panel
                  </Dropdown.Item>
                  <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Menu>
          </div>
        </Segment>
        <Sidebar.Pushable
          as={Segment}
          style={{
            marginTop: -15,
            display: "flex",
            borderRadius: 0,
            height: "calc(100% - 70px)",
          }}
        >
          <Sidebar
            as={Menu}
            visible={sideBarVisible}
            activeIndex="0"
            style={{
              flex: "0 0 150px",
              backgroundColor: "#2c3e50",
              paddingTop: 30,
              height: "100%"
            }}
            animation="slide along"
            width="thin"
            icon="labeled"
            vertical
            inverted
          >
            
            {/* {
              props.orgs &&
              props.orgs.fliter(o => o === "5dc8fe8db8e59747497e2ef4")
                .length == 1 &&
              <Menu.Item
                name="KeyManagement"
                active={activeItem === "KeyManagement"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}>
                <Icon name="car" />
                Key Management
              </Menu.Item>
            } */}
            {
              props.orgs &&
              props.orgs.filter(o => o === "5dc8fe8db8e59747497e2ef4")
                .length == 1 &&
              <Menu.Item
                name="KeyManagement"
                active={activeItem === "KeyManagement"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}
              >
                <Icon name="key" />
                Key Management
              </Menu.Item>
            }
            {
              props.orgs &&
              props.orgs.filter(o => o === "5b4efae7b89f85c29de80f08")
                .length == 1 &&
              <Menu.Item
                name="daimler"
                active={activeItem === "daimler"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}
              >
                <Icon name="car" />
                Daimler
              </Menu.Item>
            }
            {
              props.orgs &&
              props.orgs.filter(o => o === "5f0e8b94ce787bdb7b1c6a21")
                .length == 1 &&
              <Menu.Item
                name="udaan"
                active={activeItem === "udaan"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}
              >
                <Icon name="shipping fast" />
                udaan
              </Menu.Item>
            }
            {
              props.orgs &&
              props.orgs.filter(o => o == "5f59b94ad524acc50845fc79")
                .length == 1 &&
              <Menu.Item
                name="hersheys"
                active={activeItem === "hershyes"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}>
                <Icon name="shipping fast" />
                Hersheys
              </Menu.Item>
            }
            {
              props.orgs &&
              props.orgs.filter(o => o == "5f6c3b52d524acc508509b31")
                .length == 1 &&
              <Menu.Item
                name="mrRetail"
                active={activeItem === "mrRetail"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}>
                <Icon name="shopping basket" />
                Mr-Retail
              </Menu.Item>
            }
            {
              props.orgs &&
              props.orgs.filter(o => o == "5fb204f4d524acc508839783")
                .length == 1 &&
              <Menu.Item
                name="cannon"
                active={activeItem === "cannon"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}
              >
                <Icon name="camera" />
                Cannon
              </Menu.Item>
            }
            {props.orgs &&
              props.orgs.filter(o => o == "5fbcbcd07cc708707aed2410")
                .length == 1 &&
              <Menu.Item
                name="bank"
                active={activeItem === "bank"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}
              >
                <Icon name="inr" />
                ATM
              </Menu.Item>}

            {props.dash == 'dashboard' &&
              <Menu.Item
                name="dashboard"
                active={activeItem === "dashboard"}
                color="teal"
                onClick={handleItemClick}
                style={{ marginTop: 10 }}
              >
                <Icon name="dashboard" />
                Dashboard
              </Menu.Item>
            }

          </Sidebar>
          <Sidebar.Pusher style={pusherStyle}>
            <Segment
              basic
              style={{
                height: "100vh",
                display: "flex",
                padding: "10px 0px 0px 0px"
              }}
            >

              {props.dash == 'dashboard' &&
                <div>
                  {activeItem === "dashboard" && <Dashboard access={'common_dashboard'} />}
                </div>
              }
              {activeItem === "audits" && <udaan client={"Tanishq"} org={'5a0c1fd4709f16d6fe232142'} />}
              {activeItem === "agentVerification" && <udaan />}
              {activeItem === "volkswagen" && <Volkswwagen org={'5ac5ce8b3788c53c19c155e4'} />}
              {name === "daimler" && <DaimlerTemplate org={'5b4efae7b89f85c29de80f08'} token={props.token} daimlerTemplate={daimlerTemplate} />}
              {activeItem === "pandg" && <pandg org={'5b96aca9b443696fdd455599'} />}
              {name === "KeyManagement" && <KeyManagement org={"5dc8fe8db8e59747497e2ef4"} token={props.token} />}
              {activeItem === "damages" && <Damages client={"Damages"} org={'5bd2aeeb301cb0c3d9cc7ccc'} />}
              {activeItem === "preethi" && <preethi />}
              {activeItem === "godrej" && <godrej org={'5d1b5358d7646014cd5e0ed6'} />}
              {activeItem === "salesFundamentalsTemplates" && <udaan org={'5e0b1a55b4325e4ceaba3707'} />}
              {activeItem === "Furlenco" && <furlenco org={'5de7a27f0cd45a4cb475819e'} />}
              {activeItem === "johnsonAndJohnson" && <udaan org={'5e788041781f3d7b2c7dffc3'} />}
              {activeItem === "Naturals" && <Tanishq client={"Naturals"} org={'5e71eb6befe109eeb89c8659'} />}
              {activeItem === "grb" && <grb org={'5eec7fe7ce787bdb7bff1867'} />}
              {activeItem === "udaan" && <udaan org={'5f0e8b94ce787bdb7b1c6a21'} />}
              {activeItem === "hersheys" && <hersheys org={'5f59b94ad524acc50845fc79'} />}
              {activeItem === "mrRetail" && <udaan org={'5f6c3b52d524acc508509b31'} />}
              {activeItem === "cannon" && <udaan org={'5fb204f4d524acc508839783'} />}
              {name === "bank" && <Template org={'5fbcbcd07cc708707aed2410'} token={props.token} selectedTemplate={selectedTemplate} />}
            </Segment>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
        {/* {thisTemplatesView &&
        <Table columns={columns} onClick={handleTableViewAuditClick} data={templateData} />} */}
        {/* {thisTemplatesView &&
          <div>
            <h2 style={{ paddingLeft: 30, flex: "0 0 30px" }}>Templates</h2>
            <div style={{ display: "flex", flexGrow: 1, flexFlow: "column" }}>
              <div>
                <ReactTable
                  noDataText="We couldn't find anything"
                  filterable={true}
                  defaultPageSize={20}
                  sortable={true}
                  style={{ height: "85%", width: "95%", marginLeft: 30 }}
                  columns={columns}
                  onClick={handleTableViewAuditClick}
                  data={templateData.length !== 0 ? templateData : []}
                />
              </div>
            </div>
          </div>} */}

      </div>
    );
  }
};



export default Home;

