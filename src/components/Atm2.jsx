import React from 'react'
const storageBaseUrl = config["storage_base_url"];
const Atm2 = () => {

    const [EditMode, setEditMode] = useState(false);
    const [modalOpen, setmodalOpen] = useState(false);
    const [mailpopup, setmailpopup] = useState(false);


    const closeEditUser = () => {
        setmodalOpen(false)
    }
    

    const handleMail = () => {
        setmailpopup(false)
      }
      

    const editAudit= ()=>{

    }

    const onDownloadPhotos =()=>{

    }

    const onDownloadReport =()=>{

    }
    return (
        <div style={{
            marginRight: "100px",
            marginLeft: "40px",
            paddingRight: "100px"
        }}>
            <Segment
                onClick={props.onClose}
                style={{
                    color: "#808080",
                    float: "right",
                    cursor: "pointer",
                    marginTop: -7,
                    position: "absolute",
                    right: 58
                }}>
                <Icon name="arrow" className="left large" color="brown" />
            </Segment>
            <Modal
                open={modalOpen}
                onClose={closeEditUser}
                size="small"
                style={{ top: "30%", left: "10%", right: "10%", width: "50%", marginTop: "10px" }}
            >
                <Modal.Content>
                    <img
                        src={storageBaseUrl + zoomImage}
                        style={{
                            width: "550px",
                            height: "20%"
                            // padding: "0 10px"
                        }}
                    />
                </Modal.Content>
                <Modal.Actions>
                    <Button color="red" onClick={closeEditUser}>
                        <Icon name="remove" /> Close
                    </Button>
                </Modal.Actions>
            </Modal>
            <Header style={{ color: "orange", fontSize: 25 }}>
                {props.user.Audit.auditName}
            </Header>
            <p> Created at {moment(props.user.Audit.createdAt).format("DD-MM-YYYY  HH:MM:SS")}</p>
            {!EditMode &&
                <Label style={{ cursor: "pointer" }} onClick={editAudit}>
                    <Icon name="edit" /> Edit audit
                </Label>}

            {EditMode &&
                <div style={{ marginTop: "6px" }}>
                    <Label
                        style={{ cursor: "pointer", marginRight: "5px" }}
                        onClick={SaveAudit}
                    >
                        <Icon name="save" /> Save audit
                    </Label>
                    <Label
                        style={{ cursor: "pointer", marginLeft: "5px" }}
                        onClick={CancelEditAudit}
                    >
                        <Icon name="cancel" /> Cancel
                    </Label>
                </div>}
            <Label
                color="green"
                style={{ cursor: "pointer" }}
                onClick={() => onDownloadReport()}
            >
                <Icon name="table" />
                Download Report
            </Label>
            <Label
                color="blue"
                style={{ cursor: "pointer" }}
                onClick={download}
            >
                <Icon name="mail square" />
                Send Mail
            </Label>
            <Label
                color="orange"
                style={{ cursor: "pointer" }}
                onClick={() => onDownloadPhotos()}
            >
                <Icon name="camera" />
                Download Photos
            </Label>

            {mailpopup &&
                <MailPopup
                    open={mailpopup}
                    closePopup={handleMail.bind(this)}
                    sendMailAndClosePopup={handleMailchange}
                />
            }


            {props.user.Audit.questions !== undefined && props.user.Audit.questions !== null ?
                <>
                    {props.user.Audit.questions.map((ques) => (
                        <Grid>
                            <Grid.Column>
                                <Grid.Row>
                                    {ques.question}
                                </Grid.Row>
                                <Grid.Row>
                                    {ques.answerType === "text" &&
                                        <div>
                                            <Input
                                                placeholder="Enter Text"
                                                value={ques.answer}
                                            />
                                        </div>
                                    }
                                </Grid.Row>
                                <Grid.Row>
                                    {ques.answertype === "options" &&
                                        <div>
                                            <Input
                                                placeholder="Enter Text"
                                                value={ques.answer}
                                            />
                                            <br></br>
                                            <br></br>
                                            {ques.remarks &&
                                                <div>
                                                    <h4 style={{ fontWeight: 'normal' }}>{'Remarks'}</h4>
                                                    <Input
                                                        placeholder="Enter Text"
                                                        value={ques.remarks}
                                                        disabled={true}
                                                    />
                                                </div>
                                            }
                                            <br></br>
                                            <br></br>
                                            <Image.Group
                                                size="mini"
                                                style={{ cursor: "pointer" }}
                                            >
                                                {ques.photos && ques.photos.length > 0 && ques.photos.map((p, i) => {
                                                    if (p.uploadedImageUrl != null) {
                                                        return (
                                                            <Image
                                                                key={i}
                                                                src={storageBaseUrl + p.uploadedImageUrl}
                                                                onClick={() => zoomPics(p.uploadedImageUrl)}
                                                            // onClick={(k => {
                                                            // return e => this.handlePhotoClick(e, k);
                                                            // })(p)}
                                                            />
                                                        );
                                                    }
                                                })}
                                            </Image.Group>
                                        </div>
                                    }

                                    {ques.answerType === "options" &&
                                        <div>
                                            <Select
                                                disabled={!disabled}
                                                options={ques.options.map((label, i) => {
                                                    return {
                                                        value: label.value,
                                                        text: label.label,
                                                        key: label.key,
                                                    };
                                                })}
                                                placeholder={"Select any option"}
                                                value={Number(ques.answer)}
                                                onChange={(e, { value }) => atmchangeAnswer(ques, value)}
                                            />
                                            <br></br>
                                            <br></br>
                                            {ques.remarks &&
                                                <div>
                                                    <h4 style={{ fontWeight: 'normal' }}>{'Remarks'}</h4>
                                                    <Input
                                                        placeholder="Enter Text"
                                                        value={ques.remarks}
                                                        disabled={!disabled}
                                                        onChange={(e) => changeNotes(ques, e)}
                                                    />
                                                </div>
                                            }
                                            <br></br>
                                            <br></br>
                                            {!ques.remarks &&
                                                <div>
                                                    <h4 style={{ fontWeight: 'normal' }}>{'Remarks'}</h4>
                                                    <Input
                                                        placeholder="Enter Text"
                                                        disabled={!disabled}
                                                        onChange={(e) => changeNotes(ques, e)}
                                                    />
                                                </div>
                                            }
                                            <br></br>
                                            <br></br>
                                            <Image.Group
                                                size="mini"
                                                style={{ cursor: "pointer" }}
                                            >
                                                {ques.photos && ques.photos.length > 0 && ques.photos.map((p, i) => {
                                                    if (p.uploadedImageUrl != null) {
                                                        return (
                                                            <Image
                                                                key={i}
                                                                src={storageBaseUrl + p.uploadedImageUrl}
                                                                onClick={() => zoomPics(p.uploadedImageUrl)}
                                                            // onClick={(k => {
                                                            // return e => this.handlePhotoClick(e, k);
                                                            // })(p)}
                                                            />
                                                        );
                                                    }
                                                })}
                                            </Image.Group>
                                        </div>
                                    }
                                    {ques.answerType == "photo" &&
                                        <Image.Group
                                            size="mini"
                                            style={{ cursor: "pointer" }}
                                        >
                                            {ques.photos.length > 0 && ques.photos.map((p, i) => {
                                                if (p.uploadedImageUrl != null) {
                                                    return (
                                                        <Image
                                                            key={i}
                                                            src={storageBaseUrl + p.uploadedImageUrl}
                                                            onClick={() => zoomPics(p.uploadedImageUrl)}
                                                        // onClick={(k => {
                                                        // return e => this.handlePhotoClick(e, k);
                                                        // })(p)}
                                                        />
                                                    );
                                                }
                                            })}
                                        </Image.Group>
                                    }

                                    {EditMode == true &&

                                        <div>
                                            <input
                                                type="file"
                                                content="Upload additional Photos"
                                                icon="cloud upload"
                                                labelPosition="right"
                                                onChange={(e) => onPhotoSelect(e)}
                                                multiple
                                                style={{ display: "inline-block", fontSize: 20 }}
                                                accept=".jpg,.png,.jpeg"
                                            />
                                            <Button
                                                // style={{ marginTop: "10px", marginLeft: "70%" }}
                                                color="green"
                                                size="medium"
                                                onClick={(e) => uploadPhoto(ques)}
                                            >
                                                Upload
                                            </Button>
                                        </div>
                                    }


                                </Grid.Row>
                                <Divider />
                            </Grid.Column>
                        </Grid>
                    ))
                    }
                </>
                :
                null
            }

        </div>
    )
}

export default Atm2;
