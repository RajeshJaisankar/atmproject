import React from 'react'
import ReactTable from "react-table";
import selectTable from "react-table/lib/hoc/selectTable";
const CheckboxTable = selectTable(ReactTable);
const CheckboxView =(props)=> {
    return (
        <div>
            <div style={{ display: "flex", flexGrow: 1, flexFlow: "column" }}>
          <div>
            <CheckboxTable
              noDataText="We couldn't find anything"
              filterable={true}
              defaultPageSize={20}
              data={props.data}
              columns={props.columns}
              defaultFilterMethod={props.defaultFilterMethod}
              className="-striped -highlight"
//ref={r => (props.myRef(r))}
              toggleSelection={props.toggleSelection}
              selectAll={props.selectAll}
              toggleAll={props.toggleAll}
              selectType="checkbox"
              isSelected={props.isSelected}
            //  getTrProps={props.getTrProps}
            />
          </div>
        </div> 
        </div>
    )
}

export default CheckboxView
