import React from 'react'
//import config from "../config";
import {
    Segment,
    Grid,
    Dropdown,
    Statistic
} from "semantic-ui-react";
import Atm from "./Atm.jsx";
import { useState } from 'react';
import { useEffect } from 'react';
import ReactTable from 'react-table';
import CheckboxView from './CheckboxView.jsx';
import axios from 'axios'
import {
    Icon,
    Modal,
    Button,
    Step,
    Label,
    GridColumn
} from "semantic-ui-react";
import "react-notifications/lib/notifications.css";
import {
    NotificationContainer,
    NotificationManager
} from "react-notifications";
import selectTable from "react-table/lib/hoc/selectTable";

//import * as types from '../actions/types';
const CheckboxTable = selectTable(ReactTable);
const Bank = (props) => {
    const [Indata, setIndata] = useState(null);
    const [CustomerTableView, setCustomerTableView] = useState(true);
    const [selectedUser, setselectedUser] = useState({})
    const [master, setmaster] = useState([])
    const [rawMaster, setrawMaster] = useState([]);
    const [selectedState, setselectedState] = useState("");
    const [selectedCity, setselectedCity] = useState("");
    const [selectedStatus, setselectedStatus] = useState("");
    const [masterdata, setmasterdata] = useState(null);
    const [a, setA] = useState("maris")
    const [previouspage, setpreviouspage] = useState(true);
    const [count, setcount] = useState({});
    const [selectAll, setselectAll] = useState(false)
    const [modalOpen, setmodalOpen] = useState(false);
    const [selection, setselection] = useState("");
    const [userId, setUserID] = useState("");
    const [CheckboxTable, setAtmcheck] = useState(null);
    // console.log(props.token, "token in bank page")

    const setAtmCheckboxRef = (element) => {
        setAtmcheck(element);
    }
    //console.log(props.selectedTemplate, "selectedtemplate")
    // console.log(count,"count details")

    // console.log(props.Master, 'My data')
    // console.log(props.Master,"filterdata in bank");

    //console.log(props.Master.data,"bank line")
    //console.log(props.Master.Data, "   my data only................................");
    //console.log(props.templateData[0]._id,"jjbdshksbfjhv")

    // console.log(props.Atmuser,"hbdhkbhckz")
    let templateId = props.templateData[0]._id;
    // console.log(props.Master.data,"line num t65")
    //console.log(rawMaster, "jdvfsahjfkvbsfhkb")
    useEffect(() => {

        if ((props.Master.Count && Object.keys(props.Master.Count).length)) {
            //console.log(props.Master.Count, "  line num 45");
            setcount(props.Master.count);
        }

        setrawMaster(props.Master.Data);
        setmaster(props.Master.Data);



    }, [props.Master]);





    const addNewUser = () => {
     console.log(selection, 'selectionssssss');
        setmodalOpen(true)
    }



    const onStateChange = (e, Combo) => {
        setselectedState(Combo.value)
        setselectedCity("")
        setselectedStatus("")
    }
    const onStatusComboChange = (e, Combo) => {
        setselectedStatus(Combo.value)

    }

    const onCityComboChange = (e, Combo) => {
        setselectedCity(Combo.value)

    }

    const onViewDetailsClick = () => {
        //  console.log(props.selectedTemplate, "selectedtemplate  line 82 ")
        //console.log(props.selectedTemplate._id, "selectedtemplate  line 83 ")
        let state = selectedState;
        let city = selectedCity;
        let status = selectedStatus;
        console.log(state, city, status, "line number 91")
        //console.log(rawMaster,"line 90");
        // var filterMaster = [];
        // if (status === "All" && city === "All" && state === "All") {
        //     filterMaster = rawMaster;
        // }
        // else if (state !== 'ALL' && city === "All" && status !== 'All') {
        //     console.log("Data All Data")
        //     filterMaster = rawMaster.filter(obj => obj.state === state && obj.status === status);
        // }
        // else if (state !== 'All' && city === "All" && status === "All") {
        //     console.log("Data All All")
        //     filterMaster = rawMaster.filter(obj => obj.state === state);
        //     console.log(filterMaster, "line num 101");
        // }
        // else if (state !== "All" && city !== "All" && status === "All") {
        //     filterMaster = rawMaster.filter(obj => obj.state === state && obj.city === city);
        //     console.log(filterMaster, "line num 109");
        // }
        // else if (state !== "All" && city !== "All" && status === status) {
        //     filterMaster = rawMaster.filter(obj => obj.state === state && obj.city === city);
        //     console.log(filterMaster, "line num 109");
        // }
        // else {
        //     filterMaster = rawMaster.filter(obj => obj.state === state && obj.city === city && obj.status === status);
        // }
        // setmaster(filterMaster);
        filteratm(templateId, state, city, status)
    }

    const filteratm = (templateId, state, city, status) => {
        //console.log(props.token,"fddsbvhs")
        if (props.token) {
            axios
                .post("http://localhost:2014" + '/auditor/v1/atm/filter', { templateId: templateId, state: state, city: city, status: status }, {
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${props.token}`,
                    },
                }).then((response) => {
                    // console.log(response.data.Data,'filtered data');
                    setmaster(response.data.Data)
                }).catch((error) => {
                    console.log(error);
                })
        }

    }


    const AssignAudits = (locationId, UserId) => {
        console.log("rajesh", props.token)
        console.log(locationId, UserId, "line number on 167")
        if (props.token) {
            axios.post("http://localhost:2014" + '/auditor/v1/atm/assignAudits', { locationId: locationId, userId: UserId }, {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${props.token}`,
                },
            }).then((response) => {
                console.log(response, "Assign Audits");
            }).catch((error) => {
                console.log(error, "error")
            })
        }
    }

    const Onclose = () => {
        setCustomerTableView(true)
    }

    const AtmhandleTableViewAuditClick = (user) => {
        setA("suresh");
        // console.log(a,"first onclick")
        let master = [];
        setselectedUser(user)
        setCustomerTableView(false)
        if (props.token != null) {
            // setApiToken(props.token);
            axios
                .get(
                    "http://localhost:2014" + `/auditor/v1/atm/getAudit?locationId=${user._id}`,
                    {
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: `Bearer ${props.token}`,
                        },
                    }
                )
                .then((response) => {
                    //  console.log(response.data, "atmdata")

                    setmasterdata(response.data)
                })
        }
    }

    const removeDuplicates = e => {
        let newArray = [];

        // Declare an empty object
        let UniqueObject = {};

        // Loop for the array elements
        for (let i in e) {
            // Extract the title
            let objTitle = e[i]["text"];

            // Use the title as the index
            UniqueObject[objTitle] = e[i];
        }

        // Loop to push unique object into array
        for (let i in UniqueObject) {
            newArray.push(UniqueObject[i]);
        }

        return newArray;
    }

    const searchFilter = (filter, row) => {
        let value = filter.value && filter.value.toLowerCase();

        if (filter.id) {

            var flatedData = JSON.stringify(
                row._original[filter.id] ? row._original[filter.id] : ""
            );
            return flatedData.toLowerCase().includes(value);
        }
    }

    const UsertoggleSelection = (key, shift, row) => {
        // start off with the existing state
      //  console.log(key,"key in user")
        let UserId = [...userId];
        console.log(typeof(UserId))
        const keyIndexs = UserId.indexOf(key);
          console.log(keyIndexs,"ddbfhkskhfbbhkks")
        // check to see if the key exists
        if (keyIndexs >= 0) {
            // it does exist so we will remove it using destructing
            UserId = [...UserId.slice(0, keyIndexs), 
                ...UserId.slice(keyIndexs + 1)];
        } else {
            // it does not exist so add it
            UserId.push(key);
        }
        // update the state
        console.log(UserId, "dsfhbsbf")
        setUserID({ UserId });
    }

    const toggleSelection = (key, shift, row) => {
        console.log(key.slice(7), "key")
        // start off with the existing state

        let Selection = [...selection];

        const keyIndexs = Selection.indexOf(key);
        console.log(keyIndexs, "keyindex")
        // check to see if the key exists
        if (keyIndexs >= 0) {
            // it does exist so we will remove it using destructing
            Selection = [...Selection.slice(0, keyIndexs),
            ...Selection.slice(keyIndexs + 1)
            ];
        } else {
            // it does not exist so add it
            console.log(key,"else part")
            Selection.push(key);

        }
        // update the state
        console.log(Selection[0].slice(7), "selection data key")
        let select=Selection[0].slice(7);
        console.log(select,"select in toggle selection")
        setselection(select);
    }


    const toggleAll = () => {
        const SelectAll = selectAll ? false : true;
        const selection = [];
        if (SelectAll) {
            // we need to get at the internals of ReactTable
            const wrappedInstance = CheckboxTable.getWrappedInstance();
            // the 'sortedData' property contains the currently accessible records based on the filter and sort
            const currentRecords = wrappedInstance.getResolvedState().sortedData;
            // we just push all the IDs onto the selection array
            currentRecords.forEach(item => {
                selection.push(`select-${item._original._id}`);
            });
        }
        setselectAll(SelectAll);
        setselection(selection)
    }



    const isSelected = (key) => {
        // console.log(key,"hgjv")
        selection.includes(key)
    }



    const userAssign = () => {
        console.log(selection,"user assign")
        var selectedData = selection;
        var userSelection = userId;
        var locationId = [];
        var UserId = [];
        let name=selectedData.selection;
        console.log(selection, "dfhsgdfhfdsnbh")
        console.log(userSelection.UserId, "yrwiyuiwerrhwir")
        selectedData.selection.map((e) => {
            locationId.push(e.slice(7));
        });
        userSelection.UserId.map((e) => {
            UserId.push(e.slice(7));
        });
        // locationId=selectedData.selection;
        // UserId=userSelection.UserId;
        // console.log(locationId[0],UserId[0],"dbfhsdbjh")
        AssignAudits(locationId, UserId)
        setmodalOpen(false)
    }
    const columns = [
        {
            Header: "ATM Id",
            accessor: "atmId",
            width: 100,
            style: { textAlign: "left", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.atmId}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },
        {
            Header: "ATM Name",
            accessor: "atmName",
            width: 250,
            style: { textAlign: "left", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.atmName}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },

        {
            Header: "Base Branch",
            accessor: "baseBranch",
            width: 200,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.baseBranch}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },
        {
            Header: "Branch Code",
            accessor: "branchCode",
            width: 200,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.branchCode}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },

        {
            Header: "Status",
            accessor: "status",
            width: 200,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.status}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },
        {
            Header: "Assignee",
            accessor: "assigneeName",
            width: 126,
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.assigneeName}
                    onClick={() => AtmhandleTableViewAuditClick(row.original)}
                />
        },

    ];

    const column = [
        {
            Header: "Email Id's",
            accessor: "email",
            style: { textAlign: "left", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell row={row.original} text={row.original.email} />
        }
    ];



    var customers;
    if (master === undefined) {
        customers = master;
    } else {
        customers = master;
    }
    // console.log(customers, "the master")
    var Userlist = props.Atmuser;

    var city = [{ text: "All", value: "All" }];
    var state = [{ text: "All", value: "All" }];
    var status = [{ text: "All", value: "All" }];
    let uniqueCity;
    let uniqueState;
    let uniqueStatus;
    if (props.Master.Data !== undefined) {
        var data = props.Master.Data;
        data.map((e) => {
            city.push({ text: e.city, value: e.city })
            state.push({ text: e.state, value: e.state })
            status.push({ text: e.status, value: e.status })
        })
    }

    if (city.length > 0) {
        uniqueCity = removeDuplicates(city);
    }
    if (status.length > 0) {
        uniqueStatus = removeDuplicates(status);
    }
    if (state.length > 0) {
        uniqueState = removeDuplicates(state);
    }


    const closeEditUser = () => {
        setmodalOpen(false)
    }
    return (
        <div
            style={{
                flexGrow: 1,
                display: "flex",
                flexFlow: "column"
            }}
        >
            <NotificationContainer />
            {/* {CustomerTableView && */}
            {CustomerTableView &&
                <div>
                    <div style={{ padding: 15, display: "inline-block" }} >
                        <Segment
                            onClick={props.onClose}
                            style={{
                                color: "#808080",
                                float: "right",
                                cursor: "pointer",
                                marginTop: 7,
                                position: "absolute",
                                right: 58
                            }}
                        >
                            <Icon name="arrow" className="left large" color="brown" />
                        </Segment>
                        <h1
                            style={{
                                paddingLeft: 30,
                                color: "orange",
                                display: "inline-block"
                            }}
                        >
                            ATM Audit
                        </h1>
                    </div>
                    <Grid style={{ marginLeft: "30px" }}>
                        <Grid.Row columns={5}>
                            <Grid.Column>
                                <a
                                    className="ui teal left ribbon label"
                                    style={{ marginLeft: 15 }}
                                >
                                    State
                                </a>
                                <Dropdown
                                    placeholder="Select Region"
                                    fluid
                                    search
                                    selection
                                    options={uniqueState}
                                    value={selectedState}
                                    onChange={onStateChange}
                                />
                            </Grid.Column>
                            <Grid.Column>

                                <a
                                    className="ui teal left ribbon label"
                                    style={{ marginLeft: 15 }}
                                >
                                    City
                                </a>
                                <Dropdown
                                    placeholder="Select City"
                                    fluid
                                    search
                                    selection
                                    options={uniqueCity}
                                    value={selectedCity}
                                    onChange={onCityComboChange}
                                />
                            </Grid.Column>
                            <Grid.Column>
                                <a
                                    className="ui teal left ribbon label"
                                    style={{ marginLeft: 15 }}
                                >
                                    Status
                                </a>
                                <Dropdown
                                    placeholder="Select Status"
                                    fluid
                                    search
                                    selection
                                    options={uniqueStatus}
                                    value={selectedStatus}
                                    onChange={onStatusComboChange}
                                />
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <Label
                                    color="orange"
                                    style={{
                                        cursor: "pointer",
                                        padding: "10px",
                                        marginTop: "26px"
                                    }}
                                    onClick={onViewDetailsClick}
                                >
                                    View Details
                                </Label>
                            </Grid.Column>
                            <Grid.Column>
                                <Label
                                    color="green"
                                    style={{
                                        cursor: "pointer",
                                        padding: "10px",
                                        marginTop: "26px"
                                    }}
                                    tag
                                    pointing="right"
                                    onClick={addNewUser}
                                >
                                    <Icon name="add" /> Assign Audits
                                </Label>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    {/* { Object.keys(props.Master.Count).length &&
                        <div> 
                        Rajesh
                    </div>} */}
                    <div style={{ marginTop: 10 }}>
                        {/* <h1>{
                              Object.keys(count).length 
                            } </h1> */}
                        {
                            props.Master.Count !== undefined &&

                            //console.log(props.Master.Count,"ghfgcfg")
                            <Statistic.Group
                                size={"tiny"}
                                widths={Object.keys(props.Master.Count).length}
                            >
                                {
                                    Object.keys(props.Master.Count).map(key => {
                                        // console.log({ key });
                                        //console.log(props.Master.Count[key])
                                        return (

                                            <Statistic color="blue">
                                                <div style={{ fontSize: 15, marginLeft: '40%', color: "blue" }}> {key} </div>
                                                <div style={{ fontSize: 30, marginLeft: '40%', color: "blue" }}> {props.Master.Count[key]} </div>
                                            </Statistic>
                                        );
                                    })
                                }
                            </Statistic.Group>
                        }


                    </div>
                    <div
                        style={{
                            display: "flex",
                            flexGrow: 1,
                            flexFlow: "column",
                            marginTop: 20
                        }}>
                        {/* <ReactTable
                            noDataText="we couldn't find anything"
                            filterable={true}
                            defaultPageSize={20}
                            sortable={true}
                            style={{
                                height: "85%", width: "85%", marginLeft: 30
                            }}
                            columns={columns}
                            data={master.length !== 0 ? master : []}
                        /> */}
                        <CheckboxView
                            defaultFilterMethod={searchFilter}
                            data={customers}
                            columns={columns}
                            myRef={setAtmCheckboxRef}
                            toggleSelection={toggleSelection}
                            className="-striped -highlight"
                            selectAll={selectAll}
                            toggleAll={toggleAll}
                            isSelected={isSelected}
                        // getTrProps={this.rowFn}
                        />
                        {/* checkbox view*/}
                    </div>
                </div>}
            {
                !CustomerTableView && masterdata !== null &&
                <Atm token={props.token} templateData={props.Master.Data} selectedUser={selectedUser} user={masterdata} onClose={Onclose} />
            }

            <Modal style={{ top: "20%", left: "10%", right: "10%", width: "80%", marginTop: "7px" }}
                open={modalOpen}
                onClose={closeEditUser}
                size="small"
            >
                <Modal.Content>
                    {/* <ReactTable
                        defaultFilterMethod={searchFilter}
                        data={Userlist}
                        columns={column}
                        toggleSelection={UsertoggleSelection}
                        className="-striped -highlight"
                        selectAll={selectAll}
                        toggleAll={toggleAll}
                    /> */}
                    <CheckboxView
                        defaultFilterMethod={searchFilter}
                        data={Userlist}
                        columns={column}
                        myRef={setAtmCheckboxRef}
                        toggleSelection={UsertoggleSelection}
                        className="-striped -highlight"
                        selectAll={selectAll}
                        toggleAll={toggleAll}
                        isSelected={isSelected}
                    // getTrProps={this.rowFn}
                    />
                </Modal.Content>
                <Modal.Actions>
                    <Button color="red" onClick={closeEditUser}>
                        <Icon name="remove" /> Close
                    </Button>
                    <Button color="black" onClick={userAssign}>
                        Save
                    </Button>
                </Modal.Actions>
            </Modal>
        </div>
    )

}

function AuditTableCell(props) {
    function onClick() {
        props.onClick(props.row);
    }
    return (
        <div style={props.style} onClick={onClick}>
            {props.text}
        </div>
    );
}
export default Bank;