import React, { useState } from 'react'
//import "D:/office/atmproject/src/components/style.css"
//import logo from "D:/office/atmproject/src/images/logo.png"
import axios from 'axios'
import Template from "./Template"
import Home from './Home'
import { toast, SemanticToastContainer } from "react-semantic-toasts";
import { Segment, Input, Icon, Button, Modal, Form } from 'semantic-ui-react';
import { Grid, GridColumn, Message, GridRow } from 'semantic-ui-react';
import logo from "D:/office and atm/atmproject/src/images/textLogo.png"


function Login() {
    const [mail, setMail] = useState("")
    const [pwd, setPwd] = useState("")
    const [loggedin, setLoggedIn] = useState(false)
    const [token, setToken] = useState(null);
    const [name, setName] = useState("");
    const [orgs, setOrg] = useState([]);
    const [dash, setDash] = useState("");
    const [email, setemail] = useState("");
    const handleSubmit = (e) => {
        e.preventDefault();
        // alert(mail)
        setMail("")
        setPwd("")
    }



    const Submit = ( () => {
         
         axios
            .post("http://localhost:2014"+ '/auditor/v1/login', {
                email: mail,
                password: pwd,
            })
            .then((response) => {
                // console.log(response.data,"line num 28");
                // console.log(response.data.access,"dashb")
                setDash(response.data.access);
                setToken(response.data.access_token);
                setName(response.data.displayName);
                setOrg(response.data.orgs)
                setLoggedIn(true);
            })
            .catch((error) => {
                console.log(error.message, "error message")
            });

        //  console.log("prem suresh");
    });


    const SendEmail = () => {

    }

    const Close = () => {
        setemail(false)
    }

    const handleEmail = () => {

    }

    const Email1 = () => {
        console.log("Helllooo")
        setemail(true)
    }

    var loginBoxStyle = {
        borderRadius: 6,
        WebkitBoxShadow: '10px 11px 81px 3px rgba(191,191,191,1)',
        MozBoxShadow: '10px 11px 81px 3px rgba(191,191,191,1)',
        boxShadow: '10px 11px 81px 3px rgba(191,191,191,1)'
    }


    return (
        // <div>
        //     {loggedin === true ?
        //         <div className="root">
        //             <div className="main">
        //                 <div class="left">
        //                     <img className="img" src={logo} alt="logo" />
        //                     <h1>AUDIT<br></br>PRO</h1>
        //                 </div>
        //                 <div class="right">
        //                     <form onSubmit={handleSubmit}>
        //                         <input onChange={(e) => setMail(e.target.value)} value={mail} className="input" type="email" placeholder="Your Email Address" /><br />
        //                         <input onChange={(e) => setPwd(e.target.value)} value={pwd} className="input" type="password" placeholder="Password" /><br />
        //                         <button className="btn" type="submit" onClick={Submit}>Login</button><br />
        //                         <a href="/">Forgot Password?</a>
        //                     </form>
        //                 </div>
        //             </div>
        //          </div> : <Home token={token} name={name} orgs={orgs} dash={dash} />}
        //         {/* </div> : <h1> prem kumar</h1>} */}

        // </div>
        <div>
            {loggedin === false ?
                (<Grid columns='equal' verticalAlign='middle' style={{ height: '100%', marginTop: 200 }} centered>
                    <GridRow className="vcenter">
                        <Grid.Column></Grid.Column>
                        <GridColumn width={10}>
                            <Segment raised style={loginBoxStyle}>
                                <Grid columns={2}>
                                    <GridRow columns='equal' divided>
                                        <GridColumn>
                                            <img src={logo} />
                                        </GridColumn>
                                        <GridColumn verticalAlign="middle" style={{ paddingLeft: 50 }}>
                                            {/* {props.auth.loginError &&
                    <Message warning>
                      <Message.Header>Login Failed!</Message.Header>
                      <p>Your email or password doesn't look right.</p>
                    </Message>
                  } */}
                                            <Input style={{}} iconPosition='left' placeholder='Your Email Address' value={mail} onChange={(e) => setMail(e.target.value)}
                                            >
                                                <Icon name='at' />
                                                <input />
                                            </Input><br /><br />
                                            <Input iconPosition='left' type="password" placeholder='Password' value={pwd} onChange={(e) => setPwd(e.target.value)}>
                                                <Icon name='lock' />
                                                <input />
                                            </Input><br /><br />
                                            <Button primary onClick={Submit}>Login</Button>
                                            <br />
                                            <br />
                                            <p style={{ textDecorationLine: 'underline', cursor: 'pointer' }} onClick={Email1}>Forgot Password?</p>
                                        </GridColumn>
                                    </GridRow>
                                </Grid>
                            </Segment>
                        </GridColumn>
                        <GridColumn></GridColumn>
                    </GridRow>
                    {email === true &&
                        <Modal size="small" dimmer="blurring" open={email} onClose={Close}>
                            <SemanticToastContainer />
                            <Modal.Header>Enter email id</Modal.Header>
                            <Modal.Content>
                                <Form>
                                    <Input type="email" iconPosition='left' placeholder="Enter the Email" onChange={handleEmail} >
                                        <Icon name='at' />
                                        <input />
                                    </Input>
                                </Form>
                            </Modal.Content>
                            <Modal.Actions>
                                <Button color='red' onClick={Close}>
                                    Cancel
                                </Button>
                                <Button color='green' onClick={SendEmail} >
                                    Send
                                </Button>
                            </Modal.Actions>
                        </Modal>
                    }
                </Grid>) :
                (<Home token={token} name={name} orgs={orgs} dash={dash} />)
            }

        </div>
    )
}

export default Login