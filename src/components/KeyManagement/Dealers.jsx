import axios from "axios";
import React, { useEffect, useState } from "react";
import ReactTable from "react-table"
import { Icon, Segment, Modal, ModalContent, Button } from "semantic-ui-react";
import FileSaver from 'file-saver';
import DaimlerAudit from "./DaimlerAudit";
const Dealers = (props) => {
    const [modalOpen, setmodalOpen] = useState(false)
    const [auditsView, setauditsView] = useState(false);
    const [count, setCount] = useState([])
    const [dealerAudit,setdealerAudit]=useState([]);
   const [name,setname]=useState("")
   const [Data,setData]=useState([])
    const [Template,setTemplate]=useState({})
    // console.log(props.token);
    //console.log(props.selectTemplate)
    // console.log(props.dealerData);
   // console.log(props.dealerData)

    
        // setdealerData(props.dealerData);  
    
    // console.log(dealerData,"state...")
    useEffect(()=>{
           setTemplate(props.selectTemplate);
    })
    const handleDownloadAgentReportClick = (data) => {
        if (props.token) {
            axios.get("http://localhost:2014/auditor/v1/report/keymanagement?dealerId="+data._id, 
            {
                responseType: "blob",
                headers: {
                    "content-Type": "application/json",
                    Authorization: `Bearer ${props.token}`,
                },
            }).then((response)=>{
                console.log(response);
                FileSaver.saveAs(response.data,"keymanagementAuditReport.xlsx")
            }).catch((error)=>{
                console.log(error,"error")
            })
        }
    }

    const handleTableViewAuditClick = (data) => {
       // console.log(data,'......')
        setname(data.name)
        setData(data);
        if(props.token){
            axios.get("http://localhost:2014/auditor/v1/keymanagement/audits?dealerId="+data._id,{
                headers: {
                    "content-Type": "application/json",
                    Authorization: `Bearer ${props.token}`,
                },
            }).then((response)=>{
                console.log(response.data,".... audit")
                setdealerAudit(response.data);
                setauditsView(true)
            }).catch((error)=>{
                console.log(error,"error")
            })
        }
    }
    

    const onGoBack =()=>{

        setauditsView(false);
    }
    const showCount = (data) => {
        if (props.token) {
            axios.
                get("http://localhost:2014" + `/auditor/v1/keymanagement/count?dealerId=${data}`, {
                    headers: {
                        "content-Type": "application/json",
                        Authorization: `Bearer ${props.token}`,
                    },
                }).then((response) => {
                    console.log(response.data.counts, "dealercount");
                    setCount(response.data.counts);
                    setmodalOpen(true);
                }).catch((error) => {
                    console.log(error, "error")
                })
        }

    }

    const closeEdit = () => {
        console.log("close");
        setmodalOpen(false);
    }

    function filterCaseInsensitive(filter, row) {
        const id = filter.pivotId || filter.id;
        return (
            row[id] !== undefined ?
                String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
                :
                true
        );
    }
    var dealer = props.dealerData.dealers
    var columns = [
        {
            Header: "DealerCode",
            accessor: "dealerCode",
            style: { cursor: "pointer", textAlign: "center" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.dealerCode}
                    onClick={handleTableViewAuditClick}
                />
        },
        {
            Header: "DealerName",
            accessor: "name",
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.name}
                    onClick={handleTableViewAuditClick}
                />
        },
        {
            Header: 'Verification',
            accessor: 'verification',
            width: 150,
            style: { textAlign: 'center', cursor: 'pointer' },
            Cell: row => {
                if (row.original._id) {
                    return (
                        <Icon
                            size="large"
                            color="red"
                            name="calculator"
                            onClick={() => showCount(row.original._id)}
                        />
                    );
                }
                return <AuditTableCell row={row.original} text="" />;
            }
        },
        {
            Header: "Report",
            accessor: "report",
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row => {
                if (row.original._id) {
                    return (
                        <Icon
                            size="large"
                            color="green"
                            name="file excel outline"
                            onClick={() =>
                                handleDownloadAgentReportClick(row.original)}
                        />
                    );
                }
                return <AuditTableCell row={row.original} text="" />;
            }
        }
    ];

    return (
        <div style={{ flexGrow: 1, display: "flex", flexFlow: "column" }}>
            <Modal
                open={modalOpen}
                onClose={closeEdit}
                size="mini"
                style={{ marginTop: 100, marginLeft: 400 }}
            >
                <Modal.Content>
                    {
                        count != undefined &&
                        <div>
                            <h1>Total Verified:{count.totalVerified}</h1>
                            <h1>Total Vehicles:{count.totalVehicles}</h1>
                        </div>
                    }
                </Modal.Content>
                <Modal.Actions>
                    <Button color="red" onClick={closeEdit}>
                        <Icon name="remove" />Close
                    </Button>
                </Modal.Actions>
            </Modal>
            {
                !auditsView &&
                <div>
                    <Segment
                        onClick={props.onClose}
                        style={{
                            color: "red",
                            float: "right",
                            cursor: "pointer",
                            marginTop: -7,
                            position: "absolute",
                            right: 58,

                        }}
                    >
                        <Icon name="arrow" className="left large" color="green" />
                    </Segment>
                    <div>
                        <h1 style={{ paddingLeft: 30, flex: "0 0 30px" }}>Dealers</h1>
                        <div style={{ display: "flex", flexGrow: 1, flexFlow: "column" }}>
                            <div>
                                <ReactTable
                                    noDataText="we wouldn't find anything"
                                    filterable={true}
                                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                                    defaultPageSize={20}
                                    data={dealer}
                                    columns={columns} />
                            </div>
                        </div>
                    </div>
                </div>
            }
            {
                  auditsView && 
                  <DaimlerAudit onClose={onGoBack} data={Data} name={name} dealerData={props.dealerData} token={props.token} Template={Template} dealerAudit={dealerAudit}/>
            }
        </div>
    )
}
function AuditTableCell(props) {
    function onClick() {
        props.onClick(props.row);
    }
    return (
        <div style={props.style} onClick={onClick}>
            {props.text}
        </div>
    );
}
export default Dealers;