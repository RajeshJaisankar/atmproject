import React from "react";
import config from "D:/office and atm/atmproject/src/config.jsx"
import {
    Button,
    Icon,
    Modal,
    Dropdown,
    Input,
    Grid,
    Divider,
    Card,
    Label,
    Form,
    Header,
    Image
} from "semantic-ui-react";
import { uuidv4 } from "../util";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
var answer1 = [];
const storageBaseUrl = config["storage_base_url"];
const NewAudits = (props) => {
    const [toCheckPhotos, settoCheckPhotos] = useState(null);
    const [selectVehicale, setselectVehicale] = useState([]);
    const [IsbuttonClick, setIsbuttonClick] = useState(false);
    const [typedAnswer1, settypedAnswer1] = useState({});
    const [questionAnswer, setquestionAnswer] = useState({});
    const [showQuestion, setshowQuestion] = useState(true);
    const [selectPhoto, setselectPhoto] = useState([]);
    const [newDate, setDate] = useState(new Date());
    const [editMode, setEditMode] = useState(false);
    const [showPhotoSlider, setshowPhotoSlider] = useState(false);
    const [answerisSold, setanswerisSold] = useState(false);
    const [selectVehicle, setVehicle] = useState([])
    const [selectedVehicleId, setselectedVehicleId] = useState("")
    const [selectedVehicleDealerId, setselectedVehicleDealerId] = useState("")
    const [selectedVehicleForm22, setselectedVehicleForm22] = useState("")
    const [selectedVehicleForm21, setselectedVehicleForm21] = useState("")
    const [editForm21, setEditForm21] = useState("");
    const [soldDateVisible, setsoldDateVisible] = useState(false);
    const [dealerAssesmentPic, setdealerAssesmentPic] = useState(false);
    const [historyPic, sethistoryPic] = useState(false);
    const [template, setTemplate] = useState({});
    const [questions, setQuestion] = useState([])
    const [answer, setAnswer] = useState(null);
    const [dealerAssessments, setDealerAssessment] = useState([])
    const [vehicleinfo, setVehicleinfo] = useState([]);
    const [assign, setassign] = useState(null)
    const [total, setTotal] = useState([])
    const [showLoadingScreen, setshowLoadingScreen] = useState(false)
    const [qAnswer, setAnwers] = useState([]);
    console.log(props.template, "template")
    
    
    var typedAnswer = typedAnswer1;
    useEffect(() => {

        if (props.template.length != 0) {
            setTemplate(props.template);
            setQuestion(props.template.questions);
            setAnwers(props.template)
        }
        setVehicleinfo(props.Vehiclesinfo)
        
        var selectedvehicle = {};
        var answer={
            typedAnswer: typedAnswer,
            selectedvehicle: selectedvehicle
        }
        props.template.dealerAssessment.map(ques => {
            if ("answer" in ques) {
                typedAnswer[ques._id] = Number(ques.answer);
            } else {
                typedAnswer[ques._id] = ques.answer
            }
            if (ques.answerType == "photo") {
                typedAnswer[ques._id] = ques.photos
            }
        })


        setselectedVehicleForm21(props.template.vehicle ? props.template.vehicle.form21OriginalGiven : " ")
        answer = selectedVehicleForm21;
        setselectedVehicleForm22(props.template.vehicle ? props.template.vehicle.form22OriginalGiven : " ")
        answer = selectedVehicleForm22;
        // answer.selectedVehicleForm21 = props.template.vehicle ? props.template.vehicle.form21OriginalGiven : " ";
       // answer.selectedVehicleForm22 = props.template.vehicle ? props.template.vehicle.form22OriginalGiven : " ";
        
        Object.assign({},answer)
       console.log(answer,"suresh")
    }, [props.template]);

    useEffect(() => {
        if (props.template?.dealerAssessment.length != 0) {
            setDealerAssessment(props.template.dealerAssessment)
        }
    })

    const photoSliderClose = (e) => {
        setshowPhotoSlider(false);
    }

    const handleVinDropdown = (e, d) => {
        let answer = d.value.form21OriginalGiven;

        let answer22form = d.value.form22OriginalGiven;

        setselectedVehicleId(d.value._id);
        setselectedVehicleForm21(answer);
        setselectedVehicleForm22(answer22form)
        setselectedVehicleDealerId(d.value.dealerId)
    }


    const changeFrom21Data = (e) => {
        setselectedVehicleForm21(e.target.value);
    }
    
    const handleDealerAssessmentInputChange = (quesId, e) => {
        
        var ans = typedAnswer1;
        ans[quesId] = e;
        console.log(ans);
        settypedAnswer1(ans)
    }
    const handleQuestionsDateChange = (quesId, date) => {
        
        var ans = questionAnswer;
        ans[quesId] = date;
        setquestionAnswer(ans);
    }

    const handleDealerAssessmentOptionsSelect = (quesId, value) => {
        var ans = typedAnswer1;
        console.log(ans, "ans");
        ans[quesId] = value;
        if (value === 5) {
            setsoldDateVisible(true);
        }
        else if (value === 1 || value === 2 || value === 3 || value === 4) {
            setsoldDateVisible(false);
        }
        settypedAnswer1(ans);
        setshowQuestion(true);
    };

    const modalClose = () => {
        props.onVisibilityChange(false);
    }

    const handleSaveClick = () => {
       
        let audit = JSON.parse(JSON.stringify(template))
        console.log(audit, "audit in first")
        var question = questions;
     
        delete audit._id;
        
        if (props.Vehiclesinfo.vehicles) {

            props.Vehiclesinfo.vehicles.map(ID => {
               
                if (ID._id == selectedVehicleId) {
                    ID.form21OriginalGiven = selectVehicle;
                }
            });
        }
        if (!("auditId" in audit)) {
            audit.auditId = uuidv4();

        }
        if (!("vehicleId" in audit)) {
            audit.vehicleId = selectedVehicleId;
        }
        if (!("form21OriginalGiven" in audit) || "form21OriginalGiven" in audit) {
            audit.form21OriginalGiven = selectedVehicleForm21
        }
        if (!("form22OriginalGiven" in audit) || "form22OriginalGiven" in audit) {
            audit.form22OriginalGiven = selectedVehicleForm22;
        }
        if (!("createdAt") in audit) {
            audit.createdAt = new Date()
        }
        if (!("dealerId" in audit)) {
            audit.dealerId = selectedVehicleDealerId;
        }

        audit.dealerAssessment.map(ques => {
           
            if (ques.answerType === "photo") {
                if (props.Vehiclesinfo.savedPhotos !== null && dealerAssesmentPic === true) {
                   
                    props.vehiclesinfo.savedPhotos.map(photo => {
                        if (photo.newName != undefined && photo.oldName != undefined) {
                            ques.photos.push({
                                isupload: true,
                                uploadedImageUrl: photo.newName,
                                localImageUrl: photo.oldName
                            })
                        }
                    })
                }
            }
            
            ques.answer = typedAnswer[ques._id]
        });
  
        if (!("answers" in audit)) {
            audit.answers = [];
        }
        setshowLoadingScreen(true);  
        if (IsbuttonClick) {
            question.map(ques => {
                ques.answer = questionAnswer[ques._id]
                if (ques.answerType == "photo") {
                    ques.photo = [];
                   
                    if (props.vehiclesinfo.savedPhotos != null) {
                        props.vehiclesinfo.savedPhotos.map(photo => {
                            if (photo.newName != undefined && photo.oldName != undefined) {
                                ques.photo.push({
                                    isuploaded: true,
                                    uploadedImageUrl: photo.newName,
                                    localImageUrl: photo.oldName
                                })
                            }
                        })
                    }
                }
            });

            audit.answers.push({
                answers: question,
                id: uuidv4(),
                createdAt: Date()
            });
        }
        console.log(audit, "audit")
        if (props.token) {
            axios.post("http://localhost:2014/auditor/v1/keymanagement/saveAudits", audit, {
                headers: {
                    "content-Type": "application/json",
                    Authorization: `Bearer ${props.token}`,
                },
            }).then((response) => {
                console.log(response, "save audit")
            }).catch((error) => {
                console.log(error, "error");
            })
        }
       
        props.onVisibilityChange(false);
    }

    const handlePhotoClick = (e, p) => {
        setselectPhoto(p);
        setshowPhotoSlider(true);
    }


    const uploadPhoto = () => {

        var details = { orgId: props.template.orgId };
        console.log(details, "orgld")
      
        var CheckPhotos = {
            assessmentPic: true,
            historyPic: false
        }
        settoCheckPhotos(CheckPhotos)
        setshowLoadingScreen(true)

        photoAction(selectPhoto, details, CheckPhotos)
    }


    const photoAction = (data, details, tocheck) => {
        console.log(data, details, "api call before")
        console.log(tocheck, "photopooooooooooooooooooooo");

        var xhr = new XMLHttpRequest();
        var all_pictures = [];
        var to_save = [];
        for (var k = 0; k < data.length; k++) {
            all_pictures.push(data[k].name);
        }
        details.files = all_pictures;
        console.log(details, "sjfnsrb")
        if (props.token) {
            axios.
                post("http://localhost:2014/auditor/v1/storage/keyManagement/AddMultipleImages", details, {
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${props.token}`,
                    },

                }).then(function (response) {
                    var FileData = response.data.allData;
                    for (var i = 0; i < FileData.length; i++) {
                        var each_file = FileData[i];
                        for (var j = 0; j < data.length; j++) {
                            if (each_file.oldName == data[j].name) {
                                var blob = data[i].slice(0, -1, data[j].type);
                                var newFile = new File([blob], each_file.objectId, {
                                    type: data[j].type
                                });
                                to_save.push({
                                    newName: each_file.objectId,
                                    oldName: each_file.oldName
                                });

                                xhr.onreadystatechange = function () { };
                                xhr.open("PUT", each_file.url, true);
                                xhr.setRequestHeader("Content-Type", data[j].type);
                                xhr.onload = () => {
                                    if (xhr.status === 200) {
                                        to_save.push({ responseStatus: response.status });
                                    }
                                };
                                xhr.onerror = () => {
                                    console.log("error");
                                };
                                xhr.send(newFile);
                            }
                        }
                    }
                    console.log(to_save)

                }).catch((error) => {
                    console.log(error, "error")
                })
        }
    }

    const onPhotoSelect = (e, type) => {
        if (type === "dealerAssessment") {
            console.log(e.target.files, "tagert photo")
            setselectPhoto(e.target.files);
            setdealerAssesmentPic(true);
        } else if (type === "history") {
            setselectPhoto(e.target.files)
            sethistoryPic(true);
        }
    }



    const handleQuestionsInputChange = (quesId, e) => {
        console.log(quesId, e)
        var ans = questionAnswer;
        ans[quesId] = e;
        setquestionAnswer(ans);
    }

    const handleNewButtonClick = () => {
        setIsbuttonClick(true);
    }

    const changeForm22Data = (e) => {
        setselectedVehicleForm22(e.target.value)
    }
    var options = [];
   
    if (props.Vehiclesinfo.vehicles) {
        props.Vehiclesinfo.vehicles.map(vehicle => {
            options.push({
                key: vehicle._id,
                text: vehicle.vinNo,
                value: vehicle
            });
        });
    }


    return (
        <div>
            <Modal style={{ marginTop: 100, marginLeft: 200 }} open={props.isVisible} onClose={photoSliderClose}>
                <Modal.Content scrolling>
                    <Card style={{ width: "100%" }}>
                        <Card.Content>
                            <Grid>
                                <Grid.Row style={{ display: "inline-block" }}>
                                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                                        <Form.Field style={{ display: "inline-block" }}>
                                            <Header as="h5" style={{ display: "inline-block" }}>
                                                VIN Number
                                            </Header>
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column width={8} style={{ display: "inline-block" }}>

                                        {
                                            props.template.vehicle ? (
                                                <span>{props.template.vehicle.vinNo}</span>
                                            ) : (
                                                <Dropdown
                                                    style={{ display: "inline-block", width: "100%" }}
                                                    placeholder="VIN number"
                                                    onChange={handleVinDropdown}
                                                    search
                                                    selection
                                                    options={options}
                                                />
                                            )
                                        }
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                            <Grid>
                                <Grid.Row style={{ display: "inline-block" }}>
                                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                                        <Form.Field style={{ display: "inline-block" }}>
                                            <Header as="h5" style={{ display: "inline-block" }}>
                                                Form 21 Original Given
                                            </Header>
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                                        {
                                            props.template.vehicle ? (
                                                <Input
                                                    Style={{ display: "inline-block", width: "100%" }}
                                                    placeholder="choose from21 available"
                                                    value={
                                                        selectedVehicleForm21
                                                    }
                                                    onChange={changeFrom21Data}
                                                />
                                            ) : (
                                                <Input
                                                    style={{ display: "inline-block", width: "100%" }}
                                                    placeholder="choose from21 available"
                                                    value={
                                                        selectedVehicleForm21
                                                            ? selectedVehicleForm21
                                                            : ""
                                                    }
                                                    onChange={changeFrom21Data}
                                                />
                                            )
                                        }
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                            <Grid>
                                <Grid.Row style={{ display: "inline-block" }}>
                                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                                        <Form.Field style={{ dispay: "inline-block" }} >
                                            <Header as="h5" style={{ display: "inline-block" }}>
                                                Form 22 original given
                                            </Header>
                                        </Form.Field>
                                    </Grid.Column>
                                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                                        {
                                            props.template.vehicle ? (
                                                <Input
                                                    style={{ display: "inline-block", width: "100%" }}
                                                    placeholder="Choose from22 available"
                                                    value={
                                                        selectedVehicleForm22
                                                    }
                                                    onChange={changeForm22Data}
                                                />

                                            ) : (
                                                <Input
                                                    style={{ display: "inline-block", width: "100%" }}
                                                    placeholder="Choose from21 available"
                                                    value={
                                                        selectedVehicleForm22
                                                            ? selectedVehicleForm22
                                                            : ""
                                                    }
                                                    onChange={changeForm22Data}
                                                />
                                            )
                                        }
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Card.Content>
                        <Card.Content>
                            <Divider horizontal> Dealer Assessment</Divider>
                            <Form.Field>
                                <Grid>
                                    {dealerAssessments.map(question => {
                                        // console.log(question, "dattaaaaaaaaaaa")
                                        return (
                                            <Grid.Row>
                                                <Grid.Column
                                                    width={8}
                                                    style={{ display: "inline-block" }}
                                                >
                                                    {question.question}
                                                </Grid.Column>
                                                <Grid.Column width={8}>

                                                    {question.answerType == "text" && (
                                                        <Input
                                                            style={{
                                                                display: "inline-block"
                                                            }}
                                                           // value={question.answer !== undefined ? question.answer : " "}

                                                             value={typedAnswer1[question._id]}
                                                            onChange={e => {
                                                                handleDealerAssessmentInputChange(
                                                                    question._id,
                                                                    e.target.value
                                                                );
                                                            }}
                                                        />
                                                    )}
                                                    {question.answerType == "auto" && (
                                                        <Input
                                                            disabled
                                                            style={{
                                                                display: "inline-block"
                                                            }}
                                                           // value={question.answer !== undefined ? question.answer : " "}
                                                            value={typedAnswer[question._id]}
                                                            onChange={e => {
                                                                handleDealerAssessmentInputChange(
                                                                    question._id,
                                                                    e.target.value
                                                                );
                                                            }}
                                                        />
                                                    )}
                                                    {question.answerType == "date" && soldDateVisible === true && (
                                                        <Grid.Column
                                                            width={8}
                                                            style={{ display: "inline-block" }}
                                                        >
                                                            <DatePicker
                                                                //    selected={
                                                                //        question.answer !== undefined ? question.answer : " "

                                                                //    }
                                                                selected={typedAnswer1[question._id]
                                                                    ? new Date(typedAnswer1[
                                                                        question._id
                                                                    ]) : new Date()}
                                                                onChange={date => handleDealerAssessmentInputChange(question._id, date)}
                                                                monthsShown={1}
                                                                dateFormat={"dd/MM/yyyy"}
                                                                popperPlacement="bottom"
                                                                popperModifiers={{
                                                                    flip: {
                                                                        behavior: ["bottom"] // don't allow it to flip to be above
                                                                    },
                                                                    preventOverflow: {
                                                                        enabled: false // tell it not to try to stay within the view (this prevents the popper from covering the element you clicked)
                                                                    },
                                                                    hide: {
                                                                        enabled: false // turn off since needs preventOverflow to be enabled
                                                                    }
                                                                }}
                                                            />
                                                        </Grid.Column>

                                                    )}

                                                    {question.answerType == "options" && (
                                                        <Dropdown
                                                            style={{
                                                                display: "inline-block",
                                                                width: "100%"
                                                            }}
                                                            options={question.options.map((label, i) => {
                                                                return {
                                                                    value: label.value,
                                                                    text: label.label,
                                                                    key: label.value
                                                                };
                                                            })}
                                                            selection
                                                            value={typedAnswer1[question._id]}
                                                              // value={question.answer !== undefined ? question.answer : " "}
                                                            onChange={(e, data) => {
                                                                handleDealerAssessmentOptionsSelect(
                                                                    question._id,
                                                                    data.value
                                                                );
                                                            }}
                                                            placeholder={"Select any option"}
                                                        />
                                                    )}
                                                    {question.answerType == "photo" && (
                                                        <Grid.Column width={8} style={{ display: "inline-block" }}>
                                                            {question.photos.length > 0
                                                                ? question.photos.map((p, i) => {
                                                                    return (
                                                                        <Image.Group
                                                                            size="mini"
                                                                            style={{ cursor: "pointer" }}>
                                                                            <Image
                                                                                key={i}
                                                                                src={
                                                                                    storageBaseUrl +
                                                                                    p.uploadedImageUrl
                                                                                }
                                                                                onClick={(k => {
                                                                                    return e =>
                                                                                        handlePhotoClick(e, k);
                                                                                })(p)}
                                                                            />
                                                                        </Image.Group>
                                                                    );
                                                                })
                                                                :
                                                                <Grid.Column width={8} style={{ display: "inline-block" }}>
                                                                    <input
                                                                        type="file"
                                                                        content="Upload additional Photos"
                                                                        icon="cloud upload"
                                                                        labelPosition="right"
                                                                        onChange={e => onPhotoSelect(e, 'dealerAssessment')}
                                                                        multiple
                                                                        style={{ display: "inline-block" }}
                                                                        accept=".jpg,.png"
                                                                    />
                                                                    <Button
                                                                        style={{ marginTop: "10px", marginLeft: "70%" }}
                                                                        color="green"
                                                                        size="small"
                                                                        onClick={uploadPhoto}
                                                                    >
                                                                        Upload
                                                                    </Button>
                                                                </Grid.Column>}
                                                        </Grid.Column>
                                                    )}

                                                </Grid.Column>

                                            </Grid.Row>
                                        );
                                    })}
                                </Grid>
                            </Form.Field>
                        </Card.Content>
                    </Card>
                    {!answerisSold && (
                        <div style={{ marginTop: 10, display: "ïnline-block" }}>
                            {showQuestion && (
                                <Button
                                    primary
                                    onClick={handleNewButtonClick}
                                    style={{ width: "100%" }}
                                >
                                    Add New Update
                                </Button>
                            )}
                            {IsbuttonClick && (
                                <NormalQuestions
                                    onDateChange={handleQuestionsDateChange}
                                    inputChange={handleQuestionsInputChange}
                                    questions={questions}
                                    // state={this.state}
                                    onPhotoUpload={uploadPhoto}
                                    onPhotoSelect={onPhotoSelect}
                                />
                            )}
                        </div>
                    )}
                    {!answerisSold && (
                        <div>
                            <Divider horizontal>Updates</Divider>
                            <Modal
                                dimmer="blurring"
                                size="fullscreen"
                                closeOnEscape={true}
                                closeOnDimmerClick={true}
                                basic
                                open={showPhotoSlider}
                                onClose={photoSliderClose}
                            >
                                <Modal.Content>
                                    <Image
                                        fuild
                                        centered
                                        src={
                                            storageBaseUrl +
                                            selectPhoto
                                        }
                                    />
                                </Modal.Content>
                            </Modal>

                            <Card style={{ width: "100%" }}>
                                <Card.Content>
                                    <Form.Field>

                                        {template.answers ? (

                                            template.answers.map(eachAnswerGroup => {
                                               // console.log(eachAnswerGroup, "........")
                                                return (
                                                    <Grid>
                                                        {eachAnswerGroup.answers.map(question => {
                                                          //  console.log(question, "question in answer")
                                                            return (
                                                                <Grid.Row>
                                                                    <Grid.Column
                                                                        width={8}
                                                                        style={{ display: "inline-block" }}
                                                                    >
                                                                        {question.question}
                                                                    </Grid.Column>
                                                                    {question.answerType == "text" && (
                                                                        <Grid.Column
                                                                            width={8}
                                                                            style={{ display: "inline-block" }}
                                                                        >
                                                                            <Input
                                                                                style={{ display: "inline-block" }}
                                                                                value={question.answer}
                                                                            />
                                                                        </Grid.Column>
                                                                    )}
                                                                    {question.answerType == "auto" && (
                                                                        <Grid.Column
                                                                            width={8}
                                                                            style={{ display: "inline-block" }}
                                                                        >
                                                                            <Input
                                                                                disabled
                                                                                style={{ display: "inline-block" }}
                                                                                value={question.answer}
                                                                            />
                                                                        </Grid.Column>
                                                                    )}
                                                                    {question.answerType == "date" && (
                                                                        // question.answer &&
                                                                        <Grid.Column>
                                                                            <Grid.Column
                                                                                width={8}
                                                                                style={{ display: "inline-block" }}
                                                                            >
                                                                                <DatePicker
                                                                                    selected={
                                                                                        question.answer
                                                                                            ? new Date(question.answer)
                                                                                            : questionAnswer[
                                                                                            question._id
                                                                                            ]
                                                                                    }
                                                                                    monthsShown={1}
                                                                                    dateFormat={"dd/MM/yyyy"}
                                                                                    popperPlacement="bottom"
                                                                                    popperModifiers={{
                                                                                        flip: {
                                                                                            behavior: ["bottom"] // don't allow it to flip to be above
                                                                                        },
                                                                                        preventOverflow: {
                                                                                            enabled: false // tell it not to try to stay within the view (this prevents the popper from covering the element you clicked)
                                                                                        },
                                                                                        hide: {
                                                                                            enabled: false // turn off since needs preventOverflow to be enabled
                                                                                        }
                                                                                    }}
                                                                                />
                                                                            </Grid.Column>
                                                                        </Grid.Column>
                                                                    )}
                                                                    {question.answerType == "photo" && (
                                                                        <Grid.Column
                                                                            width={8}
                                                                            style={{ display: "inline-block" }}
                                                                        >
                                                                            <Image.Group
                                                                                size="mini"
                                                                                style={{ cursor: "pointer" }}
                                                                            >
                                                                                {question.photos && question.photos.map((p, i) => {
                                                                                    return (
                                                                                        <Image
                                                                                            key={i}
                                                                                            src={
                                                                                                storageBaseUrl +
                                                                                                p.uploadedImageUrl
                                                                                            }
                                                                                            onClick={(k => {
                                                                                                return e =>
                                                                                                    handlePhotoClick(e, k);
                                                                                            })(p)}
                                                                                        />
                                                                                    );
                                                                                })}
                                                                            </Image.Group>
                                                                        </Grid.Column>
                                                                    )}
                                                                </Grid.Row>
                                                            );
                                                        })}
                                                    </Grid>
                                                );
                                            })
                                        ) : (
                                            <Label color="green" style={{ textAlign: "center" }}>
                                                No updates present
                                            </Label>
                                        )}
                                    </Form.Field>
                                </Card.Content>
                            </Card>
                        </div>
                    )
                    }
                </Modal.Content>
                <Modal.Actions>
                    {
                        !editMode ? (
                            <Button color="blue" onClick={handleSaveClick}>
                                Save
                            </Button>
                        ) : (
                            <Button color="black">
                                Edit
                            </Button>
                        )
                    }

                    <Button color="red" onClick={modalClose}>
                        <Icon name="remove" />Cancel
                    </Button>
                </Modal.Actions>
            </Modal>
        </div>
    )
}

function NormalQuestions(props) {
  //  console.log(props.questions,"questions....")
    var newQuestionComponent = [];
    props.questions.map(question => {
       //  console.log(question,"normal")
        newQuestionComponent.push(
            <Grid.Row>
                <Grid.Column width={8} style={{ display: "inline-block" }}>
                    {question.question}
                </Grid.Column>

                {question.answerType == "text" && (
                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                        <Input
                            style={{ display: "inline-block" }}
                            value={question.answer}

                            onChange={e => {
                                props.inputChange(question._id, e.target.value);
                            }}
                        />
                    </Grid.Column>
                )}
                {question.answerType == "auto" && (
                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                        <Input
                            disabled
                            style={{ display: "inline-block" }}
                            value={question.answer}
                        />
                    </Grid.Column>
                )}
                {question.answerType == "date" && (
                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                        <DatePicker
                            selected={question.answer}
                            onChange={date => props.onDateChange(question._id, date)}
                            className="form-control"
                            monthsShown={1}
                            dateFormat={"dd/MM/yyyy"}
                            popperPlacement="bottom"
                            popperModifiers={{
                                flip: {
                                    behavior: ["bottom"] // don't allow it to flip to be above
                                },
                                preventOverflow: {
                                    enabled: false // tell it not to try to stay within the view (this prevents the popper from covering the element you clicked)
                                },
                                hide: {
                                    enabled: false // turn off since needs preventOverflow to be enabled
                                }
                            }}
                        />
                    </Grid.Column>
                )}

                {question.answerType === "photo" && (
                    <Grid.Column width={8} style={{ display: "inline-block" }}>
                        <input
                            type="file"
                            content="Upload additional Photos"
                            icon="cloud upload"
                            labelPosition="right"
                            onChange={e => props.onPhotoSelect(e, "history")}
                            multiple
                            style={{ display: "inline-block" }}
                            accept=".jpg,.png"
                        />
                        <Button
                            style={{ marginTop: "10px", marginLeft: "66%" }}
                            color="green"
                            size="small"
                            onClick={props.onPhotoUpload}
                        >
                            Upload
                        </Button>
                    </Grid.Column>
                )}
            </Grid.Row>
        );
    });

    return (
        <Card style={{ width: "100%" }}>
            <Card.Content>
                <Form.Field>
                    <Grid>{newQuestionComponent}</Grid>
                </Form.Field>
            </Card.Content>
        </Card>
    );
}
export default NewAudits;