import React, { useState } from "react";
import moment from "moment";
import { Segment, Icon, Label, Statistic } from "semantic-ui-react";
import { useEffect } from "react";
import NewAudits from "../KeyManagement/NewAudits"
import ReactTable from "react-table";
import axios from "axios";

const DaimlerAudit = (props) => {
  
    const [tableView, settableView] = useState(true);
    const [count, setCount] = useState({})
    const [modalOpen, setmodalOpen] = useState(false);
    const [form21,setForm21]=useState("");
    const [form22,setForm22]=useState("")
    const [audit,setAudit]=useState(null);
    const [Vehiclesinfo,setVehiclesinfo]=useState({})
    const [audits,setAudits]= useState([])
 //  console.log(props.data._id)

    useEffect(() => {
        if ((props.dealerAudit.metrics && Object.keys(props.dealerAudit.metrics).length)) {
            setCount(props.dealerAudit.metrics);
        }
        if(props.dealerAudit.audits.length !== 0){
          setAudits(props.dealerAudit.audits)
        }else
    {
      setAudits([])
    }
    }, [props.dealerAudit]);

    useEffect(()=>{
      
      if(props.token)
      {
       axios.get("http://localhost:2014//auditor/v1/keymanagement/vehicles?dealerId="+props.data._id,{
           headers: {
               "content-Type": "application/json",
               Authorization: `Bearer ${props.token}`,
           },
       }).then((response)=>{
         //  console.log(response.data,"response.....") 

           setVehiclesinfo(response.data);
       }).catch((error)=>{
           console.log(error,"error")
       })
      }
  },[])
    
    const addNewAudits = (state,audit) => {
       console.log("....",audit);
        setmodalOpen(state);
        setAudit(audit)
        
    }
    const getAuditStatusString = (assessment) => {
        var str=" ";
        let ans=assessment.answer;
        assessment.options.map((option) => {
            if (option.value == ans) {
              str = option.label
            }
          })
          return str;
       
    }
    const editForm21=(e)=>{

    if(audit !=null){
        setForm21(audit.form21OriginalGiven)
    }
    }

  const  searchFilter = (filter, row) => {
        var filterText = filter.value.toLowerCase();
        switch (filter.id) {
          case "label":
            let label = "";
            row._original.dealerAssessment.map((question) => {
              if (question._id === "5dd78d4e207b830ceeaeb3e2") {
                question.options.map((option) => {
                  if (option.value == question.answer) {
                    label = option.label;
                  }
                })
              }
            })
            return label.toLowerCase().includes(filterText);
          case "createdAt":
            return moment(row._original.createdAt).format("DD-MM-YYYY").toLowerCase().includes(filterText);
          case "engineNo":
            return row._original.vehicle.engineNo.toLowerCase().includes(filterText);
          case "vinNo":
            return row._original.vehicle.vinNo.toLowerCase().includes(filterText);
          default:
            return true;
        }
      };
    
   
    var column = [
        {
            Header: "Date",
            accessor: "createdAt",
            style: { cursor: "pointer", textAlign: "center" },
            Cell: row =>

                <AuditTableCell
                    row={row.original}
                    onRowClick={e => addNewAudits(true, e)}
                    text={moment(row.original.createdAt).format("DD-MM-YYYY")}
                />
        },
        {
            Header: "Status",
            accessor: "label",
            style: { cursor: "pointer", textAlign: "center" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    onRowClick={e => addNewAudits(true, e)}
                    text={getAuditStatusString(row.original.dealerAssessment[1])}
                />
        },
        {
            Header: "Engine No",
            accessor: "engineNo",
            style: { cursor: "pointer", textAlign: "center" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    onRowClick={e => addNewAudits(true, e)}
                    text={row.original.vehicle.engineNo}
                />
        },

        {
            Header: "VIN",
            accessor: "vinNo",
            style: { cursor: "pointer", textAlign: "center" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    onRowClick={e => addNewAudits(true, e)}
                    text={row.original.vehicle.vinNo}
                />
        }
    ];

    return (
        <div style={{ flexGrow: 1, display: "flex", flexFlow: "column" }}>
            {
                tableView &&
                <div>
                    <Segment
                        onClick={props.onClose}
                        style={{
                            color: "#808080",
                            float: "right",
                            cursor: "pointer",
                            marginTop: -7,
                            position: "absolute",
                            right: 58
                        }}
                    >
                        <Icon name="arrow" className="left large" color="brown" />
                    </Segment>
                    <div>
                        <h1 style={{ paddingLeft: 10, flex: "0 0 30px" }}>
                            Daimler Key Management -<span style={{ color: "green" }}>{props.name} </span>
                        </h1>
                        <Label
                            color="green"
                            onClick={(e) => addNewAudits(true,props.Template)}
                            tag
                            pointing="right"
                            size="large"
                            style={{ cursor: "pointer" }}
                        >
                            <Icon name="add" />Add Key Status
                        </Label>
                        {
                            props.dealerAudit.metrics !== undefined &&
                            <Statistic.Group
                                size={"tiny"}
                                widths={Object.keys(props.dealerAudit.metrics).length}
                            >
                                {
                                    Object.keys(props.dealerAudit.metrics).map(key => {
                                        return (
                                            <Statistic color="blue">
                                                <div style={{ fontSize: 12, marginLeft: '40%', color: "black" }}>{key}</div><br />
                                                <div style={{ fontSize: 25, marginLeft: '40%', color: "blue" }}> {props.dealerAudit.metrics[key]}</div>
                                            </Statistic>
                                        )
                                    })
                                }
                            </Statistic.Group>
                        }
                        {
                            modalOpen &&
                            <NewAudits
                                isVisible={modalOpen}
                                token={props.token}
                                onVisibilityChange={addNewAudits}
                                 dealerId={props.data._id}
                                 template={audit}
                                 form21={form21}
                                onForm21Change={editForm21}
                                Vehiclesinfo={Vehiclesinfo}
                            />
                        }
                        <div style={{ display: "flex", flexGrow: 1, flexFlow: "column" }}>
                            <div> 
                                <ReactTable
                                noDataText="we wouldn't find anything"
                                filterable={true}
                                defaultFilterMethod={searchFilter}
                                defaultPageSize={20}
                                 columns={column}
                                 data={audits}
                                />
                            </div>
                            </div>
                    </div>
                </div>
            }
        </div>
    )
}

function AuditTableCell(props) {
    function onClick() {
      props.onRowClick(props.row);
  
    }
    return (
      <div style={props.style} onClick={onClick}>
        {props.text}
      </div>
    );
  }
export default DaimlerAudit;