import React, { useState, useEffect } from "react";
import axios from "axios";
import { Dropdown } from "semantic-ui-react";
import ReactTable from "react-table";
import Dealers from "../KeyManagement/Dealers"

const Template = (props) => {
    const [templateView, settemplateView] = useState(true);
    const [year, setYear] = useState(new Date().getFullYear());
    const [templateData, settemplteData] = useState([]);
    const [selectTemplate, setselectTemplate] = useState([]);
    const [selectDealerData, setDealerdata] = useState([]);
    useEffect(() => {
        if (props.token) {
            axios.
                get("http://localhost:2014//auditor/v1/keymanagement/templates?year=" + year, {
                    headers: {
                        "content-Type": "application/json",
                        Authorization: `Bearer ${props.token}`,
                    },
                }).then((response) => {
                    //console.log(response, "response.....")
                    //console.log(response.data.templates,"templates")

                }).catch((error) => {
                    console.log(error, "error");
                })
        }
    }, [])
    // console.log(props.token, "token")
    // console.log(templateData[0]._id)
    const handleTableViewAuditClick = (data) => {
        console.log(data, "data")
        setselectTemplate(data);
        if (props.token) {
            axios.
                get("http://localhost:2014" + `/auditor/v1/keymanagement/dealers?templateId=` + data._id, {
                    headers: {
                        "content-Type": "application/json",
                        Authorization: `Bearer ${props.token}`,
                    },
                }).then((response) => {
                    // console.log(response.data, "response")
                    //console.log(response)
                    setDealerdata(response.data)
                }).catch((error) => {
                    console.log(error, "error")
                })
        }

        settemplateView(false);

    }

    const onTemplateChange = (e, data) => {
        // console.log(data.value,"data")
        // console.log(data,"data/....")
        if (props.token) {
            axios.
                get("http://localhost:2014/auditor/v1/keymanagement/templates?year=" + data.value, {
                    headers: {
                        "content-Type": "application/json",
                        Authorization: `Bearer ${props.token}`,
                    },
                }).then((response) => {
                    // console.log(response, "response.....")
                    console.log(response.data, "templates")
                    settemplteData(response.data.templates)
                }).catch((error) => {
                    console.log(error, "error");
                })
        }
        // console.log(".....in on")
    }
    // console.log(templateData,"data only")


    const handleClose = () => {
        settemplateView(true);
    }


    var years = [
        {
            value: 2017,
            text: "2017",
            key: "2017",
        },
        {
            value: 2018,
            text: "2018",
            key: "2018",
        },
        {
            value: 2019,
            text: "2019",
            key: "2019",
        },
        {
            value: 2020,
            text: "2020",
            key: "2020",
        },
        {
            value: 2021,
            text: "2021",
            key: "2021",
        }
    ]

    var column = [
        {
            Header: "Template Name",
            accessor: "auditName",
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.auditName}
                    onClick={handleTableViewAuditClick}
                />
        },
        {
            Header: "Vehicle count",
            accessor: "vehicleCount",
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.vehicleCount}
                    onClick={handleTableViewAuditClick}
                />
        },
        {
            Header: "DealerCount",
            accessor: "dealerCount",
            style: { textAlign: "center", cursor: "pointer" },
            Cell: row =>
                <AuditTableCell
                    row={row.original}
                    text={row.original.dealerCount}
                    onClick={handleTableViewAuditClick}
                />
        }
    ];

    return (
        <div style={{ flexGrow: 1, display: "flex", flexFlow: "column" }}>

            {templateView &&
                <div>
                    <div style={{ paddingLeft: 30, flex: "0 0 30px", display: "inline-block", marginBottom: 20 }}>
                        <h1 style={{ display: 'inline-block' }}> Templates</h1><br />
                        <Dropdown
                            style={{ display: 'inline-block' }}
                            placeholder="select year"
                            caseInsensitiveFiltering={true}
                            subStringFiltering={true}
                            fluid
                            search
                            options={years}
                            selection
                            defaultValue={year}
                            onChange={onTemplateChange}
                        />
                    </div>

                    <ReactTable
                        noDataText="we wouldn't find anything"
                        filterable={true}

                        defaultPageSize={20}
                        data={templateData}
                        columns={column}
                        handleTableViewAuditClick={handleTableViewAuditClick}

                    />
                </div>
            }
            {!templateView &&
                <div style={{ flexGrow: 1, display: "flex" }}>
                    <Dealers token={props.token} dealerData={selectDealerData} selectTemplate={selectTemplate} onClose={handleClose} />
                </div>
            }
        </div>
    )
}

function AuditTableCell(props) {
    function onClick() {
        props.onClick(props.row);
    }
    return (
        <div style={props.style} onClick={onClick}>
            {props.text}
        </div>
    );
}
export default Template;