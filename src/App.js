import Login from "./components/Login"
import  Template from './components/Template'
import {
  Switch,
  Route
} from "react-router-dom";
import ControlPanel from "../src/components/Controlplane"



function App() {
  return (
    
    <div className="App">
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/controlpanel" component={ControlPanel} />
        <Route exact path="/templates" component={Template} />
        </Switch>
         {/* <div><Login/> </div>   */}
             
             
    </div>
  );
}

export default App;
